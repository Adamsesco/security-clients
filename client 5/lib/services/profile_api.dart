import 'dart:convert';
import 'dart:io';
import 'package:security_company_app/app/apis/rest_services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart';
import 'package:security_company_app/app/common/models/client.dart' as cl;
import 'package:shared_preferences/shared_preferences.dart';

class ProfileApi {
  final Client _client = Client();
  static RestService rest = new RestService();


   get_user_infos() async {
    //ffd170ac5f883f3f935ac14164b317d0

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString("token");
    var pr_resp = await rest.get('customers/profile/$token');
    if (pr_resp['status'] == 'No Internet') {
      print('No Internet');
    } else if (pr_resp['status'] == 'error') {
      print('error');
    } else {
      print(pr_resp);
      print(pr_resp['res']['data']);
      final userData = pr_resp['res']['data'] as Map;
      return  cl.Client(
        id: userData['id'] as String,
        phoneNumber: userData['phone'] as String,
        email: userData['email'] as String,
        token: userData['token'] as String,
        firstName: userData['firstname'] as String,
        lastName: userData['lastname'] as String,
        avatarUrl: userData['avatar'] as String,
        notificationsEnabled: true,
      );

    }
  }



  Future<bool> updateProfile(
      {String firstName,
      String lastName,
      String currentPassword,
      String newPassword,
      String phone}) async {
    final body = json.encode({
      'firstname': firstName,
      'lastname': lastName,
      'current_password': currentPassword,
      'new_password': newPassword,
      'phone': phone
    });
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString("token");

    final baseUrl =
        'https://getsecured.app/backend/api/customers/editprofile/$token';
    Response response = await _client.post(baseUrl, body: body, headers: {
      HttpHeaders.acceptHeader: 'application/json',
      HttpHeaders.contentTypeHeader: 'application/json'
    });

    print(response.body);

    if (response.statusCode == 200) {
      print('updateProfile succes');
      return true;
    }
    print('updateProfile failed ${response.statusCode} ${response.body}');
    return false;
  }
}
