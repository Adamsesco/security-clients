import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:hive/hive.dart';
import 'package:security_company_app/app/common/models/sec_app.dart';
import 'package:security_company_app/app/common/themes/default_theme.dart';
import './bloc.dart';

class AppBloc extends Bloc<AppEvent, AppState> {
  @override
  AppState get initialState => AppInitial();

  @override
  Stream<AppState> mapEventToState(
    AppEvent event,
  ) async* {
    if (event is AppLaunched) {
      final app = await _getAppFromCache();
      app.launchTimesCount += 1;
      final theme = DefaultTheme();
      yield AppLoadSuccess(app: app, theme: theme);
      await app.save();
    } else {
      yield AppLoadInProgress();
    }
  }

  Future<SecApp> _getAppFromCache() async {
    final appBox = await Hive.openBox<SecApp>('app');
    var app = appBox.get('app');
    if (app == null) {
      app = SecApp.initial();
      await appBox.put('app', app);
    }
    return app;
  }
}
