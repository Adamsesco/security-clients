import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:security_company_app/app/common/models/sec_app.dart';
import 'package:security_company_app/app/common/themes/sec_theme.dart';

abstract class AppState extends Equatable {
  const AppState();
}

class AppInitial extends AppState {
  @override
  List<Object> get props => [];
}

class AppLoadInProgress extends AppState {
  @override
  List<Object> get props => [];
}

class AppLoadSuccess extends AppState {
  final SecApp app;
  final SecTheme theme;

  AppLoadSuccess({@required this.app, @required this.theme});

  @override
  List<Object> get props => [theme];
}
