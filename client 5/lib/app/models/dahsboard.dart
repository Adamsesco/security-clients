class Order {
 final String target;
 final String invoiceId;
 final  String trackid ;
 final String status;
 final  String provider;
 final String order_service;
 final String order_quantity;

 final String preorderid ;
 final  String order_service_image;
 final  String order_scheduled_time;
 final String order_start_time;
 final String order_end_time;
 final String order_address1;
 final String order_address2;
 final String order_city;
 final String order_state;
 final String order_postcode;
 final  String date_order;
 final String order_longitude;
 final String order_totalprice;
 final String order_paymentmethod;

 Order({this.date_order,
    this.invoiceId,
    this.order_address1,
    this.order_address2,
    this.order_city,
    this.order_end_time,
    this.order_longitude,
    this.order_paymentmethod,
    this.order_postcode,
    this.order_scheduled_time,
    this.order_service,
    this.order_service_image,
    this.order_start_time,
    this.order_state,
    this.order_totalprice,
    this.preorderid,
    this.provider,

   this.order_quantity,
   this.status,
    this.target,
    this.trackid});

  factory Order.fromMap(Map<String, String> map) {
    return new Order(

        invoiceId: map['invoiceId'],
        target: map['target'],
        order_address1: map['order_address1'],
        date_order: map['date_order'],
        order_address2: map['order_address2'],
        order_city: map['order_city'],
        order_end_time: map['order_end_time'],
        order_longitude: map['order_longitude'],
        order_paymentmethod: map['order_paymentmethod'],
        order_postcode: map['order_postcode'],
        order_scheduled_time: map['order_scheduled_time'],
        order_start_time: map['order_start_time'],
        order_service: map['order_service'],
        order_service_image: map['order_service_image'],
        order_state: map['order_state'],
        order_totalprice: map['order_totalprice'],
        preorderid: map['preorderid'],
        provider: map['provider'],
        trackid: map['trackid']);
  }

}
