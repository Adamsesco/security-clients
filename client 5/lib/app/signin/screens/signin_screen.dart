import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';
import 'package:security_company_app/app/common/consts/palette.dart';
import 'package:security_company_app/app/common/models/bodyguard.dart';
import 'package:security_company_app/app/common/models/client.dart';
import 'package:security_company_app/app/common/models/company.dart';
import 'package:security_company_app/app/common/widgets/sec_back_button.dart';
import 'package:security_company_app/app/common/widgets/sec_primary_button.dart';
import 'package:security_company_app/app/navigation.dart';
import 'package:security_company_app/app/routes/router.gr.dart';
import 'package:security_company_app/app/security_app/bloc/bloc.dart';
import 'package:security_company_app/app/signin/bloc/bloc.dart';
import 'package:security_company_app/app/signin/login_provider.dart';
import 'package:security_company_app/app/signin/share_preferences.dart';

class SigninScreen extends StatefulWidget {
  SigninScreen();
  //String go_to="";



  @override
  _SigninScreenState createState() => _SigninScreenState();
}

class _SigninScreenState extends State<SigninScreen> {
  SignInBloc _signInBloc;
  String _email;
  String _password;
  int _type = 0;

  final FocusNode _passwordFocusNode = FocusNode();

  @override
  void initState() {
    super.initState();

    _signInBloc = context.bloc<SignInBloc>();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<SignInBloc, SignInState>(
      listener: (BuildContext context, SignInState signInState) {
        if (signInState is SignInFailure) {
          final message = signInState.errorMessage;

          showCupertinoDialog(
            context: context,
            builder: (context) => CupertinoAlertDialog(
              title: Text('Error'),
              content: Text(message),
              actions: <Widget>[
                CupertinoDialogAction(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text('Ok'),
                ),
              ],
            ),
          );
        } else if (signInState is SignInSuccess) {
          final user = signInState.user;
          StorageUtil.inicializar().then((_) {
            var myProvider = Provider.of<LoginProvider>(context, listen: false);


            myProvider.mitexto = 'Provider';
            myProvider.isAuthenticate = true;
            StorageUtil.putBool('isAutenticado', true);

            Navigator.pop(context);

            /* if (user is Client) {
              Router.navigator.pushAndRemoveUntil(
                CupertinoPageRoute(
                  builder: (_) => CustomNavigation(),
                ),
                    (route) => false,
              );

            } else if (user is Company || user is Bodyguard) {
              Router.navigator.pushAndRemoveUntil(
                CupertinoPageRoute(
                  builder: (_) => CustomNavigation(),
                ),
                    (route) => false,
              );

            }
          });*/


          });
        }
      },
      child: BlocBuilder<AppBloc, AppState>(
        builder: (BuildContext context, AppState state) {
          final appState = state as AppLoadSuccess;

          return BlocBuilder<SignInBloc, SignInState>(
            builder: (context, signInState) {
              return Scaffold(
                body: Container(
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      colors: [Palette.sanMarino, Palette.cello],
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                    ),
                  ),
                  child: SafeArea(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(height: ScreenUtil().setHeight(10).toDouble()),
                        Row(
                          children: <Widget>[
                            SizedBox(
                                width: ScreenUtil().setWidth(29).toDouble()),
                            SecBackButton(),
                          ],
                        ),
                        SizedBox(height: ScreenUtil().setHeight(19).toDouble()),
                        Padding(
                          padding: EdgeInsetsDirectional.only(
                            start: ScreenUtil().setWidth(39).toDouble(),
                          ),
                          child: Text(
                            'Sign in',
                            style: appState.theme.titleStyle,
                          ),
                        ),
                        SizedBox(height: ScreenUtil().setHeight(28).toDouble()),
                        Expanded(
                          child: Container(
                            constraints: BoxConstraints(
                              minWidth: double.infinity,
                            ),
                            padding: EdgeInsets.symmetric(
                              horizontal: ScreenUtil().setWidth(31).toDouble(),
                              vertical: ScreenUtil().setHeight(34).toDouble(),
                            ),
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(20),
                                topRight: Radius.circular(20),
                              ),
                            ),
                            child: Column(
                              children: <Widget>[
                                Align(
                                  alignment: AlignmentDirectional.topStart,
                                  child: Text(
                                    'Welcome Back',
                                    style: appState.theme.headStyle,
                                  ),
                                ),
                                SizedBox(
                                    height:
                                        ScreenUtil().setHeight(5).toDouble()),
                                Align(
                                  alignment: AlignmentDirectional.topStart,
                                  child: Text(
                                    'Sign in to continue',
                                    style: appState.theme.subheadStyle,
                                  ),
                                ),
                                Spacer(),
                                TextField(
                                  onChanged: (String value) {
                                    _email = value;
                                  },
                                  onEditingComplete: () {
                                    FocusScope.of(context)
                                        .requestFocus(_passwordFocusNode);
                                  },
                                  keyboardType: TextInputType.emailAddress,
                                  textInputAction: TextInputAction.next,
                                  decoration: InputDecoration(
                                    contentPadding: EdgeInsets.only(
                                      bottom: ScreenUtil()
                                          .setHeight(30.5)
                                          .toDouble(),
                                    ),
                                    hintText: 'Email address',
                                    hintStyle: TextStyle(
                                      fontFamily: 'Rubik',
                                      fontSize:
                                          ScreenUtil().setSp(14).toDouble(),
                                      color: Color(0xFFB8BCC7),
                                    ),
                                    border: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                        color: Color(0xFFD8D8D8),
                                      ),
                                    ),
                                    focusedBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                        color: Color(0xFFD8D8D8),
                                      ),
                                    ),
                                    enabledBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                        color: Color(0xFFD8D8D8),
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                    height: ScreenUtil()
                                        .setHeight(30.5)
                                        .toDouble()),
                                TextField(
                                  onChanged: (String value) {
                                    _password = value;
                                  },
                                  focusNode: _passwordFocusNode,
                                  obscureText: true,
                                  decoration: InputDecoration(
                                    contentPadding: EdgeInsets.only(
                                      bottom: ScreenUtil()
                                          .setHeight(30.5)
                                          .toDouble(),
                                    ),
                                    hintText: 'Password',
                                    hintStyle: TextStyle(
                                      fontFamily: 'Rubik',
                                      fontSize:
                                          ScreenUtil().setSp(14).toDouble(),
                                      color: Color(0xFFB8BCC7),
                                    ),
                                    border: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                        color: Color(0xFFD8D8D8),
                                      ),
                                    ),
                                    focusedBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                        color: Color(0xFFD8D8D8),
                                      ),
                                    ),
                                    enabledBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                        color: Color(0xFFD8D8D8),
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                    height: ScreenUtil()
                                        .setHeight(14.5)
                                        .toDouble()),
                                Align(
                                  alignment: AlignmentDirectional.topEnd,
                                  child: Text(
                                    'Forgot Password ?',
                                    style: TextStyle(
                                      fontFamily: 'Rubik',
                                      fontSize:
                                          ScreenUtil().setSp(13).toDouble(),
                                      color: Color(0xFFB8BCC7),
                                    ),
                                  ),
                                ),
                                Spacer(flex: 3),
                                SecPrimaryButton(
                                  color: Color(0xFF4361BA),
                                  text: 'Sign in',
                                  isLoading: signInState is SignInInProgress,
                                  onTap: () {
                                    print('Signing in...');
                                    _signInBloc.add(
                                      SignInRequested(
                                          email: _email,
                                          password: _password),
                                    );
                                  },
                                ),


                                Container(height: 12,),
                                Visibility(
                                  visible: true,
                                  child: InkWell(
                                    onTap: () {
                                      Router.navigator.pushNamed(Router.signupScreen,
                                          arguments: SignupScreenArguments(user: null));
                                    },
                                    child: RichText(
                                      text: TextSpan(
                                        style: TextStyle(
                                          fontFamily: 'Rubik',
                                          fontSize:
                                              ScreenUtil().setSp(13).toDouble(),
                                          color: Color(0xFFB8BCC7),
                                        ),
                                        text: 'Don\'t have an account ? ',
                                        children: [
                                          TextSpan(
                                            text: 'Sign up',
                                            style: TextStyle(
                                              fontWeight: FontWeight.w700,
                                              color: Color(0xFF4361BA),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              );
            },
          );
        },
      ),
    );
  }
}
