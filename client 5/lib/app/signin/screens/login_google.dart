import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:provider/provider.dart';
import 'package:security_company_app/app/apis/rest_services.dart';
import 'package:security_company_app/app/common/models/client.dart';
import 'package:security_company_app/app/common/models/user.dart';
import 'package:security_company_app/app/common/widgets/sec_primary_button.dart';
import 'package:security_company_app/app/routes/router.gr.dart';
import 'package:security_company_app/app/signin/login_provider.dart';
import 'package:security_company_app/app/signin/share_preferences.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginWithGoogle extends StatefulWidget {
  LoginWithGoogle(this.returnn);
  bool returnn;

  @override
  _LoginWithGoogleState createState() => _LoginWithGoogleState();
}

class _LoginWithGoogleState extends State<LoginWithGoogle> {
  RestService rest = new RestService();
  final FirebaseMessaging _firebaseMessaging = new FirebaseMessaging();
  bool load = false;

  /* final GoogleApi _googleApi = GoogleApi(
    'google',
    '628228586526-e8gdukrojv0aq279ua4vu74ig8r6vtcd.apps.googleusercontent.com',
    'com.googleusercontent.apps.628228586526-e8gdukrojv0aq279ua4vu74ig8r6vtcd://redirect',
    clientSecret: '0LzbtSoiPMnztsGvbK-2Usc7',
    scopes: [
      'https://www.googleapis.com/auth/userinfo.email',
      'https://www.googleapis.com/auth/userinfo.profile',
    ],
  );

  Future<LoginResponse> loginWithGoogle() async {
    String token = await _firebaseMessaging.getToken();

    Account result = await _googleApi.authenticate();

    if (result == null || !result.isValid()) {
      return LoginResponse(
        user: null,
        responseCode: 0,
        message: 'Could not authenticate with Google.',
      );
    }

    final data = result.toJson();
    final tokenacees = data['refreshToken'] as String;
    final idToken = data['token'] as String;
    // print('Token: $token');
    //print('ID Token: $idToken');

    print("-------");
    print(data);


    final request = await Dio().get<String>(
        'https://getsecured.app/backend/api/customers/auth/gmail?code=$tokenacees&device_token=$token');
    print(
        'https://getsecured.app/backend/api/customers/auth/gmail?code=$tokenacees&device_token=$token');
    if (request == null ||
        request.statusCode != 200 ||
        request.data == null ||
        request.data.isEmpty) {
      return LoginResponse(
        user: null,
        responseCode: 0,
        message: 'Could not authenticate with Google.',
      );
    }

    final dynamic dataJson = jsonDecode(request.data);
    if (dataJson == null || !(dataJson is Map<String, dynamic>)) {
      return LoginResponse(
        user: null,
        responseCode: 0,
        message: 'Could not authenticate with Google.',
      );
    }

    final code = dataJson['code'] as int;
    final message = dataJson['message'] as String;
    if (code == null || message == null) {
      return LoginResponse(
        user: null,
        responseCode: 0,
        message: 'Could not authenticate with Google.',
      );
    }
    if (code != 1) {
      return LoginResponse(
        user: null,
        responseCode: code,
        message: message,
      );
    }

    final dynamic userData = dataJson['data'];

    print(userData);
    /*
    if (userData == null || !(userData is Map<String, dynamic>)) {
      return LoginResponse(
        user: null,
        responseCode: 0,
        message: 'Could not authenticate with Google.',
      );
    }

    User user; //= User.fromGoogleAuth(userData as Map<String, dynamic>);
    /*
      id: userMap['id'] as String,
      username: userMap['username'] as String,
      email: userMap['email'] as String,
      firstName: userMap['first_name'] as String,
      lastName: userMap['last_name'] as String,
      avatarUrl: userMap['avatar'] as String,
      phoneNumber: userMap['mobile'] as String,
      address: userMap['location'] as String,
      gender: genderFromCode(userMap['gender'] as String),
      languageCode: (userMap['lang'] as String)?.toUpperCase() ?? 'en',
      token: userMap['secret_tkn'] as String,
     */

    return LoginResponse(
      user: user,
      responseCode: code,
      message: message,
    );*/
  }
*/

  getStatus(GoogleSignInAccount googleuser) async {

    String token = await _firebaseMessaging.getToken();
    var pr_resp =
        await rest.get('customers/auth/gmail?email=' + googleuser.email);

    if (pr_resp['status'] == 'No Internet') {
      print('No Internet');
    } else if (pr_resp['status'] == 'error') {
      print('error');
    } else {
      setState(() {
        load = false;
      });
      if (pr_resp['res']["message"] == "Not found") {
        final user = new Client(
          email: googleuser.email,
          firstName: googleuser.displayName.split(" ")[0],
          lastName: googleuser.displayName.split(" ")[1],
          avatarUrl: googleuser.photoUrl,
          notificationsEnabled: true,
        );

        Router.navigator.pushNamed(Router.signupScreen,
            arguments: SignupScreenArguments(user: user));
      } else {
        /*
        { "id": "23", "token": "c2bb705abe11af3fcf5519db09bc9bb0", "email": "ghoste.xp12@gmail.com", "firstname": "Adams", "lastname": "Daaif", "avatar": "images\/profiles\/default.png" }
         */
        var userData = pr_resp['res']["data"];
        final user = new Client(
          id: userData['id'] as String,
          email: userData['email'] as String,
          token: userData['token'] as String,
          firstName: userData['firstname'] as String,
          lastName: userData['lastname'] as String,
          avatarUrl: userData['avatar'] as String,
          notificationsEnabled: true,
        );
        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setString("token", user.token);
        prefs.setString("id", user.id);
        prefs.setString("email", user.email);

        var myProvider = Provider.of<LoginProvider>(context, listen: false);

        myProvider.mitexto = 'Provider';
        myProvider.isAuthenticate = true;
        prefs.setBool('isAutenticado', true);

        if(widget.returnn== true)
          Navigator.of(context).pop(true);
      }
    }
  }

  final GoogleSignIn googleSignIn = GoogleSignIn(scopes: [
    //'email',
    // 'https://www.googleapis.com/auth/contacts.readonly',
  ]);

  sign_in_with_google() async {
    //  RegisterService.onLoading(context);

    setState(() {
      load = true;
    });
    GoogleSignInAccount googleUser = await googleSignIn.signIn();
    GoogleSignInAuthentication googleAuth = await googleUser.authentication;
    ;
    getStatus(googleUser);
  }

  @override
  Widget build(BuildContext context) {
    return SecPrimaryButton(
      isLoading: load,
      onTap: () {
        if(load == false) {
          sign_in_with_google();
        }

        //sign_in_with_google();
        /* Navigator.push(
                          context,
                          new MaterialPageRoute(builder:
                              (BuildContext context) {
                            return  LocationScreen(
                              searchData: SearchData(),
                              pageController: null,
                            ).wrappedRoute;
                          }));*/
        // Router.navigator.pushNamed(Router.locationScreen);
//                      _makePayment();
      },
      inverted: true,
      text: 'Sign in with Google',
      prefix: Image(
        image: AssetImage('assets/images/google-icon.png'),
      ),
    );
  }
}

class LoginResponse {
  final User user;
  final int responseCode;
  final String message;

  LoginResponse({
    @required this.user,
    @required this.responseCode,
    @required this.message,
  });
}
