import 'package:flutter/foundation.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginProvider with ChangeNotifier {

  SharedPreferences _prefs;

  LoginProvider() {
    _loadFromPrefs();
  }

  _initPrefs() async {
    if(_prefs == null)
      _prefs = await SharedPreferences.getInstance();
  }


  _loadFromPrefs() async {
    await _initPrefs();
    isAuthenticate = _prefs.getBool('isAutenticado') ==true? true: false;
    notifyListeners();
  }


  bool _isAutenticate = false;
  String _mitexto = "Provider";
  String get mitexto => _mitexto;
  bool get isAuthenticate => _isAutenticate;

  set mitexto(String newTexto) {
    _mitexto = newTexto;
    notifyListeners();
  }

  set isAuthenticate(bool newAutenticado) {
    _isAutenticate = newAutenticado;
    notifyListeners();
  }
}
