import 'dart:async';
import 'dart:io';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:security_company_app/app/signin/data/signin_repository.dart';
import './bloc.dart';

class SignInBloc extends Bloc<SignInEvent, SignInState> {
  SignInRepository signInRepository;

  SignInBloc({@required this.signInRepository});

  @override
  SignInState get initialState => InitialSignInState();

  @override
  Stream<SignInState> mapEventToState(
    SignInEvent event,
  ) async* {
    if (event is SignInRequested) {
      yield SignInInProgress();

      final result = await signInRepository.signIn(event.email, event.password);
      if (result == null) {
        yield SignInFailure(
            errorCode: 2,
            errorMessage: 'Incorrect e-mail address or password.');
      } else {
        yield SignInSuccess(user: result);
      }
    }
  }
}
