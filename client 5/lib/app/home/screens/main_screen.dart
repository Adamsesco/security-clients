import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:security_company_app/app/common/consts/palette.dart';
import 'package:security_company_app/app/common/widgets/sec_primary_button.dart';
import 'package:security_company_app/app/routes/router.gr.dart';
import 'package:security_company_app/app/signin/screens/login_google.dart';

class MainScreen extends StatelessWidget {
  MainScreen({this.returnn});
  bool returnn;


  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(context, width: 375, height: 812);

    return Scaffold(
      body: Container(
        alignment: Alignment.center,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [Palette.sanMarino, Palette.cello],
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
          ),
        ),
        child: Column(
          children: <Widget>[
            Spacer(flex: 2),
            Hero(
              tag: 'logo',
              child: Image(
                image: AssetImage('assets/images/shield.png'),
              ),
            ),
            Spacer(flex: 2),
            Padding(
              padding: EdgeInsets.symmetric(
                horizontal: ScreenUtil().setWidth(30).toDouble(),
              ),
              child: Column(
                children: <Widget>[
                  LoginWithGoogle(returnn),

                  SizedBox(height: ScreenUtil().setHeight(10).toDouble()),
                  SecPrimaryButton(
                    onTap: () {
                      if(returnn == true) {
                        Navigator.pop(context);
                      }

                      Router.navigator.pushNamed(Router.signinScreen);

                    },
                    inverted: true,
                    text: 'Sign in with email',
                  ),
                ],
              ),
            ),
            Spacer(),
            InkWell(
              onTap: () {
                if(returnn == true) {
                  Navigator.pop(context);
                }
                Router.navigator.pushNamed(Router.signupScreen,
                    arguments: SignupScreenArguments(user: null));
                },
              child: RichText(
                text: TextSpan(
                  text: 'Don\'t have an account ? ',
                  style: TextStyle(
                    fontFamily: 'Rubik',
                    fontSize: ScreenUtil().setSp(13).toDouble(),
                    color: Palette.springWood,
                  ),
                  children: [
                    TextSpan(
                      text: 'Sign up',
                      style: TextStyle(
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(height: ScreenUtil().setHeight(45).toDouble()),
          ],
        ),
      ),
    );
  }
}
