import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:security_company_app/app/navigation.dart';
import 'package:security_company_app/app/routes/router.gr.dart';
import 'package:security_company_app/app/security_app/bloc/bloc.dart';
import 'package:security_company_app/app/security_app/screens/splash_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  String token = "";

  verify_login() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (!this.mounted) return;
    setState(() {
      token =  prefs.getString("token").toString()== "null"?"": prefs.getString("token");

    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    verify_login();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<AppBloc, AppState>(
      condition: (previousState, currentState) {
        return (previousState is AppInitial ||
                previousState is AppLoadInProgress) &&
            currentState is AppLoadSuccess;
      },
      listener: (context, appState) {
        if (appState is AppLoadSuccess) {
          final app = appState.app;

          print("------------------------");
          print(token);
          if (app.launchTimesCount == 1) {
            Router.navigator.pushReplacementNamed(Router.introScreen);
          } else /*if (token != "") */{
            // Router.navigator.pushReplacementNamed(Router.clientDashboardScreen);
            Router.navigator.pushReplacement(
              CupertinoPageRoute(
                builder: (_) => CustomNavigation(),
              ),
            );
          }

        }
      },
      child: SplashScreen(),
    );
  }
}
