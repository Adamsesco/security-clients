import 'dart:convert';
//import 'package:flutter_inappbrowser/flutter_inappbrowser.dart';

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:security_company_app/app/apis/rest_services.dart';
import 'package:security_company_app/app/common/consts/palette.dart';
import 'package:security_company_app/app/common/helpers/theme_helpers.dart';
import 'package:security_company_app/app/common/models/bodyguard.dart';
import 'package:security_company_app/app/common/models/company.dart';
import 'package:security_company_app/app/common/models/search_data.dart';
import 'package:security_company_app/app/common/models/user.dart';
import 'package:security_company_app/app/common/widgets/expandible_container.dart';
import 'package:security_company_app/app/common/widgets/sec_back_button.dart';
import 'package:security_company_app/app/common/widgets/sec_screen_subtitle.dart';
import 'package:security_company_app/app/navigation.dart';
import 'package:security_company_app/app/security_app/bloc/bloc.dart';
import 'package:security_company_app/app/routes/router.gr.dart';
import 'package:security_company_app/app/signin/login_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:latlong/latlong.dart';

class SearchResultsScreen extends StatefulWidget {
  final SearchData searchData;

  SearchResultsScreen({Key key, @required this.searchData}) : super(key: key);

  @override
  _SearchResultsScreenState createState() => _SearchResultsScreenState();
}

class _SearchResultsScreenState extends State<SearchResultsScreen> {
  bool _expanded = false;
  Future<List<User>> _results;
  int _expandedIndex;
  bool load = false;
  String track = "";
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  Future<List<User>> _search() async {
    print('searching...');
    print(widget.searchData.address["city"]);

    final formData = FormData.fromMap({
      'city': widget.searchData.address["city"],
      'userid': '2',
      'serviceid': widget.searchData.service.id,
      'quantity': widget.searchData.guardsCount.toString(),
    });
    final request = await Dio().post<String>(
        'https://getsecured.app/backend/api/search',
        data: formData);

    if (request == null ||
        request.statusCode != 200 ||
        request.data == null ||
        request.data.isEmpty) {
      return [];
    }

    print(request.data);
    final json = jsonDecode(request.data);
    final resultsData = json['results'];
    if (resultsData == null) {
      return [];
    }

    final results = <User>[];

    final providers = resultsData['providers'] as List<dynamic>;
    if (providers != null) {
      providers.forEach((providerData) {
        final provider = Company(
          id: providerData['id'] as String,
          name: providerData['provider_name'] as String,
          avatarUrl: providerData['logo'] as String,
          rating: double.tryParse(providerData['rate'] as String) ?? 0.0,
          price: double.tryParse(providerData['coast'] as String) ?? 0.0,
        );

        results.add(provider);
      });
    }

    final agents = resultsData['agents'] as List<dynamic>;
    if (agents != null) {
      print("yesss");
      print(agents);
      agents.forEach((agentData) {
        print(
            "---------------------------------------------------------------------------");
        print(agentData);
        final agent = Bodyguard(
          id: agentData['id'] as String,
          name: agentData['agent_name'] as String,
          role: agentData['role'] as String,
          avatarUrl: agentData['logo'] as String,
          rating: double.tryParse(agentData['rate'] as String) ?? 0.0,
          price: double.tryParse(agentData['coast'] as String) ?? 0.0,
        );

        results.add(agent);
      });
    }

    return results;
  }

  ccc() async {
    var b = await rest.get("checkPayment/" + track);

    print(b);
    if (b["res"]["message"] != "Not payed") {
      Alert(
        context: context,
        type: AlertType.success,
        title: "Successfully completed",
        desc: "Successfully completed",
        buttons: [
          DialogButton(
            // color: Fonts,
            child: Text(
              "Return",
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
            onPressed: () {
              Router.navigator.pushAndRemoveUntil(
                CupertinoPageRoute(
                  builder: (_) => CustomNavigation(),
                ),
                    (route) => false,
              );
            },
            width: 120,
          )
        ],
      ).show();
    } else {
      _scaffoldKey.currentState.showSnackBar(SnackBar(
          backgroundColor: b["res"]["message"].toString() != "Not payed"
              ? Colors.green
              : Colors.red[800],
          content: new Text(
            b["res"]["message"].toString() != "Not payed"
                ? "Paid Successful"
                : "Order not paid ",
            style:
                TextStyle(color: Colors.white, fontSize: 16, fontFamily: "arb"),
          )));
    }
  }

  bool lod = false;
  static RestService rest = new RestService();
  bool repeat = true;

  loadi() {
    setState(() {
      lod = false;
    });
  }

  pay_order(address, total, p_name, p_id) async {
    if (repeat == false) {
    } else {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String token = prefs.getString("token");

      setState(() {
        lod = true;
      });

      final js = {
        "service_id": widget.searchData.service.id.toString(),
        "providerAgent": p_name.toString(),
        "provider_id": p_id.toString(),
        "scheduled_time": DateTime.now().toString(),
        "start_time": widget.searchData.dateTimeFrom.toString(),
        "end_time": widget.searchData.dateTimeTo.toString(),
        "address1": widget.searchData.address["address"],
        "address2": "",
        "city": widget.searchData.address["city"],
        "state": widget.searchData.address["code"],
        "postcode": widget.searchData.address["postcode"],
        "longitude": widget.searchData.location.latitude.toString(),
        "latitude": widget.searchData.location.longitude.toString(),
        "totalprice": total.toString(),
        "quantity": widget.searchData.guardsCount.toString(),
        "paymentmethod": "1"
      };

      print(js);

      var b = await rest.post("customers/makeOrder/$token", js);

      track = b["trackid"].toString() as String;
      ChromeSafariBrowser browser =
          new MyChromeSafariBrowser(new InAppBrowser(), ccc, loadi);
      browser.open(
          url: "https://getsecured.app/backend/makepayment/?token=$track");

      /// https://getsecured.app/backend/api/checkPayment/1593298354
      ///
      //var res = await rest.get("makepayment/?token=$track");
      //print(res);
      // ChromeSafariBrowser browser =
      // new MyChromeSafariBrowser(new InAppBrowser(), ccc, load);

    }
    setState(() {
      repeat = false;
    });
  }

  @override
  void initState() {
    super.initState();

    _results = _search();
  }

  @override
  Widget build(BuildContext context) {
    var myProvider = Provider.of<LoginProvider>(context);

    return BlocBuilder<AppBloc, AppState>(
      builder: (context, appState) {
        final theme = getThemeFromState(appState);

        return Scaffold(
          key: _scaffoldKey,
          body: Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/images/background.png'),
                fit: BoxFit.cover,
                alignment: Alignment.topRight,
              ),
              color: Palette.mystic,
            ),
            child: SafeArea(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(height: ScreenUtil().setHeight(8).toDouble()),
                  Container(
                    margin: EdgeInsets.symmetric(
                      horizontal: ScreenUtil().setWidth(15).toDouble(),
                    ),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: Align(
                            alignment: AlignmentDirectional.centerStart,
                            child: SecBackButton(),
                          ),
                        ),
                        Expanded(
                          child: Center(
                            child: Text(
                              'LIST',
                              style: theme.screenTitleStyle,
                            ),
                          ),
                        ),
                        Expanded(
                          child: Align(
                            alignment: AlignmentDirectional.centerEnd,
                            child: InkWell(
                              splashColor: Colors.transparent,
                              highlightColor: Colors.transparent,
                              child: Container(
                                padding: EdgeInsets.symmetric(
                                  horizontal:
                                      ScreenUtil().setWidth(11).toDouble(),
                                  vertical:
                                      ScreenUtil().setWidth(12).toDouble(),
                                ),
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: Palette.springWood,
                                ),
                                child: Image.asset(
                                  'assets/images/filters-icon.png',
                                  width: ScreenUtil().setWidth(23).toDouble(),
                                  height: ScreenUtil().setWidth(21).toDouble(),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: ScreenUtil().setHeight(23).toDouble()),
                  SecScreenSubtitle(
                    subtitle: 'At vero eos censes',
                  ),
                  SizedBox(height: ScreenUtil().setHeight(15.32).toDouble()),
                  Container(
                    margin: EdgeInsets.symmetric(
                      horizontal: ScreenUtil().setWidth(15).toDouble(),
                    ),
                    child: Text(
                      'At vero eos censes aut odit aut quid percipit aut rerum necessitatibus saepe.',
                      style: theme.bodyTextStyle,
                    ),
                  ),
                  SizedBox(height: ScreenUtil().setHeight(16).toDouble()),
                  Expanded(
                    child: FutureBuilder<List<User>>(
                      future: _results,
                      builder: (context, snapshot) {
                        switch (snapshot.connectionState) {
                          case ConnectionState.none:
                          case ConnectionState.waiting:
                          case ConnectionState.active:
                            return Center(child: CupertinoActivityIndicator());

                          case ConnectionState.done:
                            if (snapshot.hasError || snapshot.data == null) {
                              return Container();
                            }

                            final results = snapshot.data;

                            return ListView.separated(
                              itemCount: results.length,
                              separatorBuilder: (_, __) => SizedBox(
                                  height:
                                      ScreenUtil().setHeight(10).toDouble()),
                              itemBuilder: (BuildContext context, int index) {
                                dynamic result = results[index];
                                double a = result.price as double;
                                String total = (widget.searchData.dateTimeTo
                                            .difference(
                                                widget.searchData.dateTimeFrom)
                                            .inDays *
                                        widget.searchData.guardsCount *
                                        a)
                                    .toString();
                                return InkWell(
                                  onTap: () {
                                    setState(() {
                                      _expandedIndex = index;
                                    });
                                  },
                                  child: Container(
                                    margin: EdgeInsets.symmetric(
                                      horizontal:
                                          ScreenUtil().setWidth(8).toDouble(),
                                    ),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(12),
                                      color: Palette.springWood,
                                      boxShadow: [
                                        BoxShadow(
                                          color: Palette.sanMarino
                                              .withOpacity(0.22),
                                          offset: Offset(1, 1),
                                          blurRadius: 3,
                                          spreadRadius: -1,
                                        ),
                                      ],
                                    ),
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(12),
                                      child: Column(
                                        children: <Widget>[
                                          Container(
                                            margin: EdgeInsetsDirectional.only(
                                              top: ScreenUtil()
                                                  .setHeight(16)
                                                  .toDouble(),
                                              end: ScreenUtil()
                                                  .setWidth(32)
                                                  .toDouble(),
                                              bottom: ScreenUtil()
                                                  .setHeight(13)
                                                  .toDouble(),
                                              start: ScreenUtil()
                                                  .setWidth(24)
                                                  .toDouble(),
                                            ),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: <Widget>[
                                                Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: <Widget>[
                                                    Text(
                                                      result.name
                                                          .toString()
                                                          .toUpperCase(),
                                                      style: theme
                                                          .companyNameStyle,
                                                    ),
                                                    SizedBox(
                                                        height: ScreenUtil()
                                                            .setHeight(5)
                                                            .toDouble()),
                                                    SizedBox(
                                                        height: ScreenUtil()
                                                            .setHeight(18)
                                                            .toDouble()),
                                                    RichText(
                                                      text: TextSpan(
                                                        style: theme.priceStyle,
                                                        text:
                                                            '\$${result.price.toString()}',
                                                        children: [
                                                          TextSpan(
                                                            text: '/Day',
                                                            style: theme
                                                                .perDayStyle,
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                                Container(
                                                  width: ScreenUtil()
                                                      .setWidth(65)
                                                      .toDouble(),
                                                  height: ScreenUtil()
                                                      .setWidth(65)
                                                      .toDouble(),
                                                  decoration: BoxDecoration(
                                                    image: DecorationImage(
                                                      image: NetworkImage(result
                                                          .avatarUrl
                                                          .toString()),
                                                      fit: BoxFit.contain,
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                          ExpandibleContainer(
                                            expanded: _expandedIndex == index,
                                            duration: const Duration(
                                                milliseconds: 400),
                                            child: Column(
                                              children: <Widget>[
                                                SizedBox(
                                                    height: ScreenUtil()
                                                        .setHeight(32 - 13)
                                                        .toDouble()),
                                                Padding(
                                                  padding: EdgeInsets.symmetric(
                                                    horizontal: ScreenUtil()
                                                        .setWidth(30)
                                                        .toDouble(),
                                                  ),
                                                  child: Column(
                                                    children: <Widget>[
                                                      Row(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        children: <Widget>[
                                                          Expanded(
                                                            flex: 1,
                                                            child: Text(
                                                              'Order ID',
                                                              style: theme
                                                                  .resultDetailsTitleStyle,
                                                            ),
                                                          ),
                                                          Expanded(
                                                            flex: 2,
                                                            child: Text(
                                                              result.id
                                                                  .toString(),
                                                              style: theme
                                                                  .resultDetailsContentStyle,
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                      SizedBox(
                                                          height: ScreenUtil()
                                                              .setHeight(7)
                                                              .toDouble()),
                                                      Row(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        children: <Widget>[
                                                          Expanded(
                                                            flex: 1,
                                                            child: Text(
                                                              'Order type',
                                                              style: theme
                                                                  .resultDetailsTitleStyle,
                                                            ),
                                                          ),
                                                          Expanded(
                                                            flex: 2,
                                                            child: Text(
                                                              widget.searchData
                                                                  .service.name,
                                                              style: theme
                                                                  .resultDetailsContentStyle,
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                      SizedBox(
                                                          height: ScreenUtil()
                                                              .setHeight(7)
                                                              .toDouble()),
                                                      Row(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        children: <Widget>[
                                                          Expanded(
                                                            flex: 1,
                                                            child: Text(
                                                              'Order mission',
                                                              style: theme
                                                                  .resultDetailsTitleStyle,
                                                            ),
                                                          ),
                                                          Expanded(
                                                            flex: 2,
                                                            child: Text(
                                                              result.role
                                                                  .toString(),
                                                              style: theme
                                                                  .resultDetailsContentStyle,
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                      SizedBox(
                                                          height: ScreenUtil()
                                                              .setHeight(7)
                                                              .toDouble()),
                                                      Row(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        children: <Widget>[
                                                          Expanded(
                                                            flex: 1,
                                                            child: Text(
                                                              'Nº of Guards',
                                                              style: theme
                                                                  .resultDetailsTitleStyle,
                                                            ),
                                                          ),
                                                          Expanded(
                                                            flex: 2,
                                                            child: Text(
                                                              widget.searchData
                                                                  .guardsCount
                                                                  .toString(),
                                                              style: theme
                                                                  .resultDetailsContentStyle,
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                      SizedBox(
                                                          height: ScreenUtil()
                                                              .setHeight(7)
                                                              .toDouble()),
                                                      Row(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        children: <Widget>[
                                                          Expanded(
                                                            flex: 1,
                                                            child: Text(
                                                              'From',
                                                              style: theme
                                                                  .resultDetailsTitleStyle,
                                                            ),
                                                          ),
                                                          Expanded(
                                                            flex: 2,
                                                            child: Text(
                                                              DateFormat(
                                                                      'dd E y')
                                                                  .format(widget
                                                                      .searchData
                                                                      .dateTimeFrom),
                                                              // '17 Jan 2020',
                                                              style: theme
                                                                  .resultDetailsContentStyle,
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                      SizedBox(
                                                          height: ScreenUtil()
                                                              .setHeight(7)
                                                              .toDouble()),
                                                      Row(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        children: <Widget>[
                                                          Expanded(
                                                            flex: 1,
                                                            child: Text(
                                                              'To',
                                                              style: theme
                                                                  .resultDetailsTitleStyle,
                                                            ),
                                                          ),
                                                          Expanded(
                                                            flex: 2,
                                                            child: Text(
                                                              DateFormat(
                                                                      'dd E y')
                                                                  .format(widget
                                                                      .searchData
                                                                      .dateTimeTo),
                                                              style: theme
                                                                  .resultDetailsContentStyle,
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                      SizedBox(
                                                          height: ScreenUtil()
                                                              .setHeight(7)
                                                              .toDouble()),
                                                      Row(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        children: <Widget>[
                                                          Expanded(
                                                            flex: 1,
                                                            child: Text(
                                                              'Address',
                                                              style: theme
                                                                  .resultDetailsTitleStyle,
                                                            ),
                                                          ),
                                                          Expanded(
                                                            flex: 2,
                                                            child: Text(
                                                              widget.searchData
                                                                      .address[
                                                                  "address"],
                                                              style: theme
                                                                  .resultDetailsContentStyle,
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                                SizedBox(
                                                    height: ScreenUtil()
                                                        .setHeight(42)
                                                        .toDouble()),
                                                InkWell(
                                                  onTap: () {
                                                    print('Pay now');

                                                    if (myProvider
                                                            .isAuthenticate ==
                                                        false) {
                                                      Router.navigator
                                                          .pushNamed(Router.mainScreen,arguments:MainScreenArguments(returnn: true) );
                                                    } else
                                                      pay_order(
                                                          "",
                                                          total,
                                                          result.name
                                                              .toString(),
                                                          result.id
                                                              .toString()
                                                              .toUpperCase());
                                                  },
                                                  child: Container(
                                                    decoration: BoxDecoration(
                                                      color: Palette.sanMarino,
                                                    ),
                                                    padding:
                                                        EdgeInsetsDirectional
                                                            .only(
                                                      top: ScreenUtil()
                                                          .setHeight(22)
                                                          .toDouble(),
                                                      end: ScreenUtil()
                                                          .setWidth(21)
                                                          .toDouble(),
                                                      bottom: ScreenUtil()
                                                          .setHeight(20)
                                                          .toDouble(),
                                                      start: ScreenUtil()
                                                          .setWidth(21)
                                                          .toDouble(),
                                                    ),
                                                    child: Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .spaceBetween,
                                                      children: <Widget>[
                                                        RichText(
                                                          text: TextSpan(
                                                            style: theme
                                                                .totalOrderStyle,
                                                            text:
                                                                'Total order  ',
                                                            children: [
                                                              TextSpan(
                                                                text: '\$' +
                                                                    total,
                                                                style: theme
                                                                    .payPriceStyle,
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                        lod
                                                            ? CupertinoTheme(
                                                                data: CupertinoTheme.of(
                                                                        context)
                                                                    .copyWith(
                                                                        brightness:
                                                                            Brightness.dark),
                                                                child:
                                                                    CupertinoActivityIndicator(),
                                                              )
                                                            : Text(
                                                                'Pay now',
                                                                style: theme
                                                                    .payNowStyle,
                                                              ),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                );
                              },
                            );
                        }
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}

class MyChromeSafariBrowser extends ChromeSafariBrowser {
  MyChromeSafariBrowser(browserFallback, this.ccc, this.load) : super();

  var ccc;
  var load;

  @override
  void onOpened() {
    load();
  }

  @override
  void onLoaded() {
    print("ChromeSafari browser loaded");
    load();
  }

  @override
  void onClosed() {
    print("ChromeSafari browser closed");
    ccc();
  }
}
