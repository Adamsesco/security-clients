import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:security_company_app/app/common/consts/palette.dart';
import 'package:security_company_app/app/common/helpers/theme_helpers.dart';
import 'package:security_company_app/app/common/models/guard_type.dart';
import 'package:security_company_app/app/common/models/search_data.dart';
import 'package:security_company_app/app/common/widgets/sec_back_button.dart';
import 'package:security_company_app/app/common/widgets/sec_primary_button.dart';
import 'package:security_company_app/app/common/widgets/sec_screen_subtitle.dart';
import 'package:security_company_app/app/routes/router.gr.dart';
import 'package:security_company_app/app/security_app/bloc/bloc.dart';

class GuardsNumberScreen extends StatefulWidget {
  final SearchData searchData;

  GuardsNumberScreen({Key key, @required this.searchData}) : super(key: key);

  @override
  _GuardsNumberScreenState createState() => _GuardsNumberScreenState();
}

class _GuardsNumberScreenState extends State<GuardsNumberScreen> {
  GuardType _guardType;
  int _guardsCount = 1;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AppBloc, AppState>(
      builder: (context, appState) {
        final theme = getThemeFromState(appState);

        return Scaffold(
          body: Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/images/background.png'),
                fit: BoxFit.cover,
                alignment: Alignment.topRight,
              ),
              color: Palette.mystic,
            ),
            child: SafeArea(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(height: ScreenUtil().setHeight(8).toDouble()),
                  Container(
                    margin: EdgeInsets.symmetric(
                      horizontal: ScreenUtil().setWidth(15).toDouble(),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        SecBackButton(),
                        Text(
                          'NUMBER',
                          style: theme.screenTitleStyle,
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: ScreenUtil().setHeight(23).toDouble()),
                  SecScreenSubtitle(
                    subtitle: 'At vero eos censes',
                  ),
                  SizedBox(height: ScreenUtil().setHeight(15.32).toDouble()),
                  Container(
                    margin: EdgeInsets.symmetric(
                      horizontal: ScreenUtil().setWidth(15).toDouble(),
                    ),
                    child: Text(
                      'At vero eos censes aut odit aut quid percipit aut rerum necessitatibus saepe.',
                      style: theme.bodyTextStyle,
                    ),
                  ),
                  SizedBox(height: ScreenUtil().setHeight(100).toDouble()),
                  Container(
                    margin: EdgeInsets.symmetric(
                      horizontal: ScreenUtil().setWidth(12).toDouble(),
                    ),
                    padding: EdgeInsets.only(
                      top: ScreenUtil().setHeight(41).toDouble(),
                      right: ScreenUtil().setWidth(21).toDouble(),
                      bottom: ScreenUtil().setHeight(41).toDouble(),
                      left: ScreenUtil().setWidth(21).toDouble(),
                    ),
                    decoration: BoxDecoration(
                      color: Palette.springWood,
                      borderRadius: BorderRadius.circular(17),
                      boxShadow: [
                        BoxShadow(
                          color: Palette.sanMarino.withOpacity(0.22),
                          offset: Offset(1, 1),
                          blurRadius: 3,
                          spreadRadius: -3,
                        ),
                      ],
                    ),
                    child: Column(
                      children: <Widget>[
                        // Builder(
                        //   builder: (context) {
                        //     final isSelected =
                        //         _guardType == GuardType.securityGuard;

                        //     return InkWell(
                        //       onTap: () {
                        //         setState(() {
                        //           _guardType = GuardType.securityGuard;
                        //         });
                        //       },
                        //       child: Container(
                        //         padding: EdgeInsetsDirectional.only(
                        //           top: ScreenUtil().setHeight(23).toDouble(),
                        //           end: ScreenUtil().setWidth(20).toDouble(),
                        //           bottom: ScreenUtil().setHeight(23).toDouble(),
                        //           start: ScreenUtil().setWidth(36).toDouble(),
                        //         ),
                        //         decoration: BoxDecoration(
                        //           borderRadius: BorderRadius.circular(11),
                        //           color: isSelected
                        //               ? Palette.sanMarino
                        //               : Palette.seashell,
                        //         ),
                        //         child: Row(
                        //           mainAxisAlignment:
                        //               MainAxisAlignment.spaceBetween,
                        //           children: <Widget>[
                        //             Text(
                        //               'SECURITY GUARD',
                        //               style: isSelected
                        //                   ? theme.selectedGuardTypeStyle
                        //                   : theme.guardTypeStyle,
                        //             ),
                        //             if (isSelected)
                        //               Image.asset(
                        //                 'assets/images/selected-icon.png',
                        //                 width: ScreenUtil()
                        //                     .setWidth(22)
                        //                     .toDouble(),
                        //                 height: ScreenUtil()
                        //                     .setWidth(22)
                        //                     .toDouble(),
                        //               ),
                        //             if (!isSelected)
                        //               Container(
                        //                 width: ScreenUtil()
                        //                     .setWidth(22)
                        //                     .toDouble(),
                        //                 height: ScreenUtil()
                        //                     .setWidth(22)
                        //                     .toDouble(),
                        //                 decoration: BoxDecoration(
                        //                   shape: BoxShape.circle,
                        //                   border: Border.all(
                        //                     color: Palette.mercury,
                        //                     width: 2,
                        //                   ),
                        //                 ),
                        //               ),
                        //           ],
                        //         ),
                        //       ),
                        //     );
                        //   },
                        // ),
                        // SizedBox(height: ScreenUtil().setHeight(17).toDouble()),
                        // Builder(
                        //   builder: (context) {
                        //     final isSelected =
                        //         _guardType == GuardType.bodyGuard;

                        //     return InkWell(
                        //       onTap: () {
                        //         setState(() {
                        //           _guardType = GuardType.bodyGuard;
                        //         });
                        //       },
                        //       child: Container(
                        //         padding: EdgeInsetsDirectional.only(
                        //           top: ScreenUtil().setHeight(23).toDouble(),
                        //           end: ScreenUtil().setWidth(20).toDouble(),
                        //           bottom: ScreenUtil().setHeight(23).toDouble(),
                        //           start: ScreenUtil().setWidth(36).toDouble(),
                        //         ),
                        //         decoration: BoxDecoration(
                        //           borderRadius: BorderRadius.circular(11),
                        //           color: isSelected
                        //               ? Palette.sanMarino
                        //               : Palette.seashell,
                        //         ),
                        //         child: Row(
                        //           mainAxisAlignment:
                        //               MainAxisAlignment.spaceBetween,
                        //           children: <Widget>[
                        //             Text(
                        //               'BODYGUARD',
                        //               style: isSelected
                        //                   ? theme.selectedGuardTypeStyle
                        //                   : theme.guardTypeStyle,
                        //             ),
                        //             if (isSelected)
                        //               Image.asset(
                        //                 'assets/images/selected-icon.png',
                        //                 width: ScreenUtil()
                        //                     .setWidth(22)
                        //                     .toDouble(),
                        //                 height: ScreenUtil()
                        //                     .setWidth(22)
                        //                     .toDouble(),
                        //               ),
                        //             if (!isSelected)
                        //               Container(
                        //                 width: ScreenUtil()
                        //                     .setWidth(22)
                        //                     .toDouble(),
                        //                 height: ScreenUtil()
                        //                     .setWidth(22)
                        //                     .toDouble(),
                        //                 decoration: BoxDecoration(
                        //                   shape: BoxShape.circle,
                        //                   border: Border.all(
                        //                     color: Palette.mercury,
                        //                     width: 2,
                        //                   ),
                        //                 ),
                        //               ),
                        //           ],
                        //         ),
                        //       ),
                        //     );
                        //   },
                        // ),
                        // SizedBox(height: ScreenUtil().setHeight(51).toDouble()),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            InkWell(
                              onTap: () {
                                if (_guardsCount > 1) {
                                  setState(() {
                                    _guardsCount -= 1;
                                  });
                                }
                              },
                              child: Image.asset(
                                'assets/images/remove-guard-icon.png',
                                width: ScreenUtil().setWidth(78).toDouble(),
                                height: ScreenUtil().setWidth(78).toDouble(),
                              ),
                            ),
                            Container(
                              width: ScreenUtil().setWidth(104).toDouble(),
                              height: ScreenUtil().setWidth(104).toDouble(),
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Palette.wildSand,
                              ),
                              child: Text(
                                _guardsCount.toString(),
                                style: theme.guardsNumberStyle,
                              ),
                            ),
                            InkWell(
                              onTap: () {
                                setState(() {
                                  _guardsCount += 1;
                                });
                              },
                              child: Image.asset(
                                'assets/images/add-guard-icon.png',
                                width: ScreenUtil().setWidth(78).toDouble(),
                                height: ScreenUtil().setWidth(78).toDouble(),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Spacer(),
                  Container(
                    margin: EdgeInsets.symmetric(
                      horizontal: ScreenUtil().setWidth(30).toDouble(),
                    ),
                    child: SecPrimaryButton(
                      onTap: () {
                        widget.searchData.guardType = _guardType;
                        widget.searchData.guardsCount = _guardsCount;
                        Router.navigator.pushNamed(
                          Router.searchResultsScreen,
                          arguments: SearchResultsScreenArguments(
                              searchData: widget.searchData),
                        );
                      },
                      enabled: _guardsCount >= 1,
                      text: 'Next',
                      disabledColor: Palette.iron,
                    ),
                  ),
                  SizedBox(height: ScreenUtil().setHeight(23).toDouble()),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
