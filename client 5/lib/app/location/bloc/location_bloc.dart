import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:flutter/services.dart';
import 'package:geolocator/geolocator.dart';
import './bloc.dart';

class LocationBloc extends Bloc<LocationEvent, LocationState> {
  final Geolocator _geolocator = Geolocator();

  @override
  LocationState get initialState => InitialLocationState();

  @override
  Stream<LocationState> mapEventToState(
    LocationEvent event,
  ) async* {
    if (event is LocationRequested) {
      yield LocationLoadInProgress();

      final lastLocation = await _getLastKnownLocation();
      if (lastLocation == null) {
        yield LocationUpdateFailure();
      } else {
        final lastAddress = await _getAddress(lastLocation);
        yield LocationUpdateSuccess(
            location: lastLocation, address: lastAddress);

        final currentLocation = await _getCurrentLocation();
        if (currentLocation != null) {
          final currentAddress = await _getAddress(currentLocation);
          yield LocationUpdateSuccess(
              location: currentLocation, address: currentAddress);
        }
      }
    } else if (event is LocationChanged) {
      yield LocationLoadInProgress();
      final address = await _getAddress(event.location);
      yield LocationUpdateSuccess(location: event.location, address: address);
    }
  }

  Future<Position> _getLastKnownLocation() async {
    Position position;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      position = await _geolocator.getLastKnownPosition(
          desiredAccuracy: LocationAccuracy.best);
    } on PlatformException {
      position = null;
    }

    return position;
  }

  Future<Position> _getCurrentLocation() async {
    Position position;

    try {
      position = await _geolocator.getCurrentPosition(
          desiredAccuracy: LocationAccuracy.best);
    } catch (e) {
      position = null;
    }

    return position;
  }

  Future<Map<String,String>> _getAddress(Position location) async {
    final placeMarks = await _geolocator.placemarkFromPosition(location);
    if (placeMarks == null || placeMarks.isEmpty) {
      return null;
    }

    final placeMark = placeMarks[0];
    final country = placeMark.country ?? '';
    final city = placeMark.locality ?? '';
    final postcode = placeMark.postalCode ?? '';
    final code = placeMark.isoCountryCode ?? '';

    final region = placeMark.administrativeArea;
    final street = placeMark.thoroughfare ?? '';
    final placeName = placeMark.name;

    final address = '$street $placeName, $city, $region. $country';
    return {
      "address": address,
      "place":placeName,
      "city": city,
      "street": street,
      "country": country,
      "region": region,
      "postcode": postcode,
      "code": code
    };
  }
}
