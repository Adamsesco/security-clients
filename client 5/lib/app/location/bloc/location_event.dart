import 'package:equatable/equatable.dart';
import 'package:geolocator/geolocator.dart';
import 'package:meta/meta.dart';

abstract class LocationEvent extends Equatable {
  const LocationEvent();
}

class LocationRequested extends LocationEvent {
  @override
  List<Object> get props => [];
}

class LocationChanged extends LocationEvent {
  final Position location;

  LocationChanged({@required this.location});

  @override
  List<Object> get props => [location];
}
