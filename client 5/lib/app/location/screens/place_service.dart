import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart';

class Place {
  String streetNumber;
  String street;
  String city;
  String zipCode;
  double lat;
  double lng;


  Place({
    this.streetNumber,
    this.street,
    this.city,
    this.zipCode,
    this.lat,
    this.lng
  });

  @override
  String toString() {
    return 'Place(streetNumber: $streetNumber, street: $street, city: $city, zipCode: $zipCode)';
  }
}

class Suggestion {
  final String placeId;
  final String description;

  Suggestion(this.placeId, this.description);

  @override
  String toString() {
    return 'Suggestion(description: $description, placeId: $placeId)';
  }
}

class PlaceApiProvider {
  final client = Client();

  PlaceApiProvider(this.sessionToken);

  final sessionToken;

  static final String androidKey = 'AIzaSyCESijRK1ROlUvqjEEG6vtCyRoMjjClzpM';
  static final String iosKey = 'AIzaSyCESijRK1ROlUvqjEEG6vtCyRoMjjClzpM';
  final apiKey = Platform.isAndroid ? androidKey : iosKey;


  Future<Map<String, dynamic>> decodeAndSelectPlace(String placeId) async{
     Map<String,dynamic> location = {
       "lat":0.0,
       "lng":0.0
     };

    String endpoint =
        "https://maps.googleapis.com/maps/api/place/details/json?key=${apiKey}" +
            "&placeid=$placeId";

    final response =  await  client.get(endpoint);
      if (response.statusCode == 200) {

        print("---------------------------------------------------");
        print( response.body);
        print("---------------------------------------------------");

        location["lat"] =
        jsonDecode(response.body)['result']['geometry']['location']["lat"] as double ;
        location["lng"] =
        jsonDecode(response.body)['result']['geometry']['location']["lng"] as double ;

      //  LatLng latLng = LatLng(location['lat'], location['lng']);

      }

      return location;
  }



  Future fetchSuggestions(String input, String lang) async {
    final request =
        'https://maps.googleapis.com/maps/api/place/autocomplete/json?input=$input&types=address&language=en&key=$apiKey&sessiontoken=$sessionToken';


    final response = await client.get(request);



    if (response.statusCode == 200) {
      final result = json.decode(response.body);



      if (result['status'] == 'OK') {


        // compose suggestions in a list


        final list = result['predictions']  ;



       /* print(list);
        for(Suggestion i in list){
          print(i.description);
        }*/
        return list
            .map((p) =>
                Suggestion(p['place_id'] as String, p['description'] as String))
            .toList();
      }
      if (result['status'] == 'ZERO_RESULTS') {
        return [];
      }
      throw Exception(result['error_message']);
    } else {
      throw Exception('Failed to fetch suggestion');
    }
  }

  Future<Place> getPlaceDetailFromId(String placeId) async {
    final request =
        'https://maps.googleapis.com/maps/api/place/details/json?place_id=$placeId&fields=address_component&key=$apiKey&sessiontoken=$sessionToken';
    final response = await client.get(request);

    if (response.statusCode == 200) {
      final result = json.decode(response.body);
      if (result['status'] == 'OK') {

        print(result['result']);

        final components =
            result['result']['address_components'] as List<dynamic>;
        // build result
        final place = Place();
        components.forEach((c) {
          final List type = c['types'] as List;
          if (type.contains('street_number')) {
            place.streetNumber = c['long_name'] as String;
          }
          if (type.contains('route')) {
            place.street = c['long_name'] as String;
          }
         /* if (type.contains('locality')) {
            place.city = c['long_name'] as String;
          }
          if (type.contains('postal_code')) {
            place.zipCode = c['long_name'] as String;
          }*/
        });
        Map<String,dynamic> location = await decodeAndSelectPlace(placeId);
        place.lat= location["lat"] as double;
        place.lng = location["lng"] as double;

        return place;
      }
      throw Exception(result['error_message']);
    } else {
      throw Exception('Failed to fetch suggestion');
    }
  }
}
