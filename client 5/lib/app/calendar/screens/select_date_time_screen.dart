import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:security_company_app/app/calendar/widgets/sec_calendar.dart';
import 'package:security_company_app/app/calendar/widgets/sec_time_picker.dart';
import 'package:security_company_app/app/common/consts/palette.dart';
import 'package:security_company_app/app/common/helpers/theme_helpers.dart';
import 'package:security_company_app/app/common/models/search_data.dart';
import 'package:security_company_app/app/common/widgets/sec_back_button.dart';
import 'package:security_company_app/app/common/widgets/sec_primary_button.dart';
import 'package:security_company_app/app/routes/router.gr.dart';
import 'package:security_company_app/app/security_app/bloc/bloc.dart';

class SelectDateTimeScreen extends StatefulWidget {
  final SearchData searchData;

  SelectDateTimeScreen({Key key, @required this.searchData}) : super(key: key);

  @override
  _SelectDateTimeScreenState createState() => _SelectDateTimeScreenState();
}

class _SelectDateTimeScreenState extends State<SelectDateTimeScreen> {
  DateTime _dateTimeFrom;
  DateTime _dateTimeTo;
  DateTime _currentDate;

  @override
  void initState() {
    super.initState();

    _currentDate = DateTime.now();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AppBloc, AppState>(
      builder: (context, appState) {
        final theme = getThemeFromState(appState);

        return Scaffold(
          body: Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/images/background.png'),
                fit: BoxFit.cover,
                alignment: Alignment.topRight,
              ),
              color: Palette.mystic,
            ),
            child: SafeArea(
              child: Column(
                children: <Widget>[
                  SizedBox(height: ScreenUtil().setHeight(8).toDouble()),
                  Container(
                    margin: EdgeInsets.symmetric(
                      horizontal: ScreenUtil().setWidth(15).toDouble(),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        SecBackButton(),
                        Text(
                          'DATE & TIME',
                          style: theme.screenTitleStyle,
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: ScreenUtil().setHeight(23).toDouble()),
                  Expanded(
                    child: Stack(
                      children: <Widget>[
                        Visibility(
                          visible: false,
                          maintainSize: true,
                          maintainState: true,
                          maintainAnimation: true,
                          child: Column(
                            children: <Widget>[
                              Text(
                                DateFormat('EEEE').format(DateTime.now()),
                                style: theme.dateScreenDayStyle,
                              ),
                              SizedBox(
                                  height: ScreenUtil().setHeight(3).toDouble()),
                              Text(
                                DateFormat('dd MMMM').format(DateTime.now()),
                                style: theme.dateScreenDateStyle,
                              ),
                            ],
                          ),
                        ),
                        Row(
                          children: <Widget>[
                            Expanded(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  if (_dateTimeFrom == null)
                                    Text(
                                      'From',
                                      style: theme.dateScreenDateStyle.copyWith(
                                        color: Colors.red,
                                      ),
                                    ),
                                  if (_dateTimeFrom != null) ...[
                                    Text(
                                      DateFormat('EEEE').format(_dateTimeFrom),
                                      style: theme.dateScreenDayStyle,
                                    ),
                                    SizedBox(
                                        height: ScreenUtil()
                                            .setHeight(3)
                                            .toDouble()),
                                    Text(
                                      DateFormat('dd MMMM')
                                          .format(_dateTimeFrom),
                                      style: theme.dateScreenDateStyle,
                                    ),
                                  ],
                                ],
                              ),
                            ),
                            Image.asset('assets/images/date-to-icon.png'),
                            Expanded(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  if (_dateTimeTo == null)
                                    Text(
                                      'To',
                                      style: theme.dateScreenDateStyle.copyWith(
                                        color: Colors.red,
                                      ),
                                    ),
                                  if (_dateTimeTo != null) ...[
                                    Text(
                                      DateFormat('EEEE').format(_dateTimeTo),
                                      style: theme.dateScreenDayStyle,
                                    ),
                                    SizedBox(
                                        height: ScreenUtil()
                                            .setHeight(3)
                                            .toDouble()),
                                    Text(
                                      DateFormat('dd MMMM').format(_dateTimeTo),
                                      style: theme.dateScreenDateStyle,
                                    ),
                                  ],
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Spacer(),
                  SecCalendar(
                    currentDate: _currentDate,
                    onDateChanged: (dateFrom, dateTo) {
                      setState(
                        () {
                          if (dateFrom == null) {
                            _dateTimeFrom = null;
                          } else if (_dateTimeFrom == null) {
                            _dateTimeFrom = dateFrom;
                          } else {
                            _dateTimeFrom = DateTime(
                                dateFrom.year,
                                dateFrom.month,
                                dateFrom.day,
                                _dateTimeFrom.hour,
                                _dateTimeFrom.minute);
                          }

                          if (dateTo == null) {
                            _dateTimeTo = null;
                          } else if (_dateTimeTo == null) {
                            _dateTimeTo = dateTo;
                          } else {
                            _dateTimeTo = DateTime(
                                dateTo.year,
                                dateTo.month,
                                dateTo.day,
                                _dateTimeTo.hour,
                                _dateTimeTo.minute);
                          }
                        },
                      );
                    },
                  ),
                  Spacer(),
                  SecTimePicker(currentDate: _currentDate),
                  Spacer(),
                  Container(
                    margin: EdgeInsets.symmetric(
                      horizontal: ScreenUtil().setWidth(30).toDouble(),
                    ),
                    child: SecPrimaryButton(
                      onTap: () {
                        widget.searchData.dateTimeFrom = _dateTimeFrom;
                        widget.searchData.dateTimeTo = _dateTimeTo;
                        Router.navigator.pushNamed(
                          Router.guardsNumberScreen,
                          arguments: GuardsNumberScreenArguments(
                            searchData: widget.searchData,
                          ),
                        );
                      },
                      enabled: _dateTimeFrom != null && _dateTimeTo != null,
                      text: 'Next',
                      disabledColor: Palette.iron,
                    ),
                  ),
                  SizedBox(height: ScreenUtil().setHeight(8).toDouble()),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
