import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';
import 'package:security_company_app/app/common/helpers/theme_helpers.dart';
import 'package:security_company_app/app/common/models/search_data.dart';
import 'package:security_company_app/app/common/widgets/sec_navigation_bar.dart';
import 'package:security_company_app/app/dashboard/screens/client_dashboard_screen.dart';
import 'package:security_company_app/app/home/screens/main_screen.dart';
import 'package:security_company_app/app/location/screens/location_screen.dart';
import 'package:security_company_app/app/notifications/screens/notifications_screen.dart';
import 'package:security_company_app/app/security_app/bloc/bloc.dart';
import 'package:security_company_app/app/settings/screens/settings_screen.dart';
import 'package:security_company_app/app/settings/settings_navigator.dart';
import 'package:security_company_app/app/signin/login_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CustomNavigation extends StatefulWidget {
  @override
  _CustomNavigationState createState() => _CustomNavigationState();
}

class _CustomNavigationState extends State<CustomNavigation> {
  String id = "";
  final PageController pageController = PageController(initialPage: 0);




  @override
  Widget build(BuildContext context) {

    var myProvider = Provider.of<LoginProvider>(context);


    return BlocBuilder<AppBloc, AppState>(
      builder: (context, appState) {
        final theme = getThemeFromState(appState);
        return Material(
          child: Stack(
            children: <Widget>[
              PageView.builder(
                physics: NeverScrollableScrollPhysics(),
                controller: pageController,
                itemBuilder: (context, int index) {
                  switch (index) {
                    case 0:
                      return LocationScreen(
                        searchData: SearchData(),
                        pageController: pageController,
                      ).wrappedRoute;
                    case 1:
                      return myProvider.isAuthenticate == false
                          ? MainScreen(returnn: false,)
                          :  ClientDashboardScreen(
                        pageController: pageController,
                      );

                    case 2:
                      return myProvider.isAuthenticate == false
                          ? MainScreen(returnn: false,)
                          : NotificationsScreen();
                    case 3:
                      return myProvider.isAuthenticate == false
                          ? MainScreen(returnn: false,)
                          :  SettingsNavigator(
                        pageController: pageController,
                      );
                    default:
                      return SizedBox();
                  }
                },
              ),
              Positioned(
                bottom: 0,
                child: SecNavigationBar(
                  pageController: pageController,
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
