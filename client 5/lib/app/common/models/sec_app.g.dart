// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sec_app.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class SecAppAdapter extends TypeAdapter<SecApp> {
  @override
  final typeId = 0;

  @override
  SecApp read(BinaryReader reader) {
    var numOfFields = reader.readByte();
    var fields = <int, dynamic>{
      for (var i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return SecApp(
      launchTimesCount: fields[0] as int,
    );
  }

  @override
  void write(BinaryWriter writer, SecApp obj) {
    writer
      ..writeByte(1)
      ..writeByte(0)
      ..write(obj.launchTimesCount);
  }
}
