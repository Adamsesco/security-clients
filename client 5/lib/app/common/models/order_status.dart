enum OrderStatus {
  newOrder,
  pending,
  inProgress,
  done,
  delayed,
  canceled,
}
