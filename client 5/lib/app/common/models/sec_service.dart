import 'package:flutter/material.dart';

class SecService {
  String id;
  String name;
  String description;
  Widget icon;
  bool isVisible;

  SecService({this.id, this.name, this.description, this.icon, this.isVisible});
}
