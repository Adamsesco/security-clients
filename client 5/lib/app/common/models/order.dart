import 'package:geolocator/geolocator.dart';
import 'package:security_company_app/app/common/models/bodyguard.dart';
import 'package:security_company_app/app/common/models/client.dart';
import 'package:security_company_app/app/common/models/company.dart';
import 'package:security_company_app/app/common/models/guard_type.dart';
import 'package:security_company_app/app/common/models/order_status.dart';
import 'package:security_company_app/app/common/models/sec_service.dart';

class Order {
  final String id;
  final OrderStatus status;
  final Company company;
  final Bodyguard bodyguard;
  final Client client;
  final Position location;
  final String address;
  final SecService service;
  final DateTime dateCreated;
  final DateTime dateFrom;
  final DateTime dateTo;
  final GuardType guardType;
  final int guardsCount;
  final double price;

  Order(
      {this.id,
      this.status,
      this.company,
      this.bodyguard,
      this.client,
      this.location,
      this.address,
      this.service,
      this.dateCreated,
      this.dateFrom,
      this.dateTo,
      this.guardType,
      this.guardsCount,
      this.price});

  factory Order.fromMap(Map<String, dynamic> map) {
    return Order();
  }
}
