import 'package:meta/meta.dart';
import 'package:security_company_app/app/common/models/order.dart';

class SecNotification {
  final String id;
  //final SecNotificationType type;
  final String text; //title
  final String description;//description
  final DateTime date;//fate
  bool read;//status

  SecNotification(
      {@required this.id,
    //  @required this.type,
      @required this.text,
      @required this.date,
      this.description,
      this.read = false});
}

enum SecNotificationType {
  orderNew,
  orderDone,
  orderDelayed,
  orderCanceled,
  custom,
}
