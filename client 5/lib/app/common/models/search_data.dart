import 'package:geolocator/geolocator.dart';
import 'package:security_company_app/app/common/models/guard_type.dart';
import 'package:security_company_app/app/common/models/sec_service.dart';
import 'package:security_company_app/app/common/models/user.dart';

class SearchData {
  Position location;
  Map<String, String> address;
  SecService service;
  DateTime dateTimeFrom;
  DateTime dateTimeTo;
  GuardType guardType;
  int guardsCount;
  User provider;
}