import 'package:flutter/material.dart';

class ExpandibleContainer extends StatefulWidget {
  final Widget child;
  final AlignmentGeometry alignment;
  final EdgeInsetsGeometry padding;
  final Decoration decoration;
  final BoxConstraints constraints;
  final EdgeInsetsGeometry margin;
  final Matrix4 transform;
  final Duration duration;
  final Curve curve;
  final bool expanded;

  ExpandibleContainer(
      {Key key,
      this.child,
      this.alignment,
      this.padding,
      this.decoration,
      this.constraints,
      this.margin,
      this.transform,
      this.duration,
      this.curve,
      this.expanded = false})
      : super(key: key);

  @override
  _ExpandibleContainerState createState() => _ExpandibleContainerState();
}

class _ExpandibleContainerState extends State<ExpandibleContainer>
    with SingleTickerProviderStateMixin {
  Animation<double> _animation;
  AnimationController _animationController;
  bool _expanded;

  @override
  void initState() {
    super.initState();

    _expanded = widget.expanded;

    _animationController =
        AnimationController(duration: widget.duration, vsync: this);
    _animation = CurvedAnimation(parent: _animationController, curve: Curves.ease)
      ..addListener(() {
        setState(() {});
      });
  }

  @override
  void dispose() {
    _animationController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (_expanded != widget.expanded) {
      _expanded = widget.expanded;
      if (_animationController.isAnimating) {
        _animationController.stop();
      }
      if (_expanded) {
        _animationController.forward();
      } else {
        _animationController.reverse();
      }
    }

    return SizeTransition(
      axis: Axis.vertical,
      sizeFactor: _animation,
      child: Container(
        alignment: widget.alignment,
        padding: widget.padding,
        decoration: widget.decoration,
        constraints: widget.constraints,
        margin: widget.margin,
        transform: widget.transform,
        child: widget.child,
      ),
    );
  }
}
