import 'package:flutter/painting.dart';

abstract class SecTheme {
  String get logoFullPath;

  String get logoShortPath;

  Color get primaryColor;

  Color get secondaryColor;

  TextStyle get hintStyle;

  TextStyle get textFieldStyle;

  TextStyle get link1Style;

  TextStyle get link2Style;

  TextStyle get buttonTextStyle;

  TextStyle get titleStyle;

  TextStyle get subtitleStyle;

  TextStyle get headStyle;

  TextStyle get subheadStyle;

  TextStyle get introHeadStyle;

  TextStyle get introTextStyle;

  TextStyle get introButtonTextStyle;

  TextStyle get screenTitleStyle;

  TextStyle get screenSubtitleStyle;

  TextStyle get screenSubtitleSmallStyle;

  TextStyle get bodyTextStyle;

  TextStyle get serviceNameStyle;

  TextStyle get serviceDescriptionStyle;

  TextStyle get subServiceNameStyle;

  TextStyle get subServiceDescriptionStyle;

  TextStyle get selectedSubServiceNameStyle;

  TextStyle get selectedSubServiceDescriptionStyle;

  TextStyle get calendarWeekDayStyle;

  TextStyle get calendarEnabledDayStyle;

  TextStyle get calenderSelectedDayStyle;

  TextStyle get calendarDisabledDayStyle;

  TextStyle get dateScreenDayStyle;

  TextStyle get dateScreenDateStyle;

  TextStyle get timePickerTextStyle;

  TextStyle get selectedGuardTypeStyle;

  TextStyle get guardTypeStyle;

  TextStyle get guardsNumberStyle;

  TextStyle get companyNameStyle;

  TextStyle get distanceStyle;

  TextStyle get priceStyle;

  TextStyle get perDayStyle;

  TextStyle get resultDetailsTitleStyle;

  TextStyle get resultDetailsContentStyle;

  TextStyle get totalOrderStyle;

  TextStyle get payPriceStyle;

  TextStyle get payNowStyle;

  TextStyle get clientPaidSumStyle;

  TextStyle get clientTotalOrdersStyle;

  TextStyle get filterTextStyle;

  TextStyle get orderDateHead;

  TextStyle get orderCardCompanyName;

  TextStyle get orderCardServiceName;

  TextStyle get orderCardDate;

  TextStyle get orderCardPrice;

  TextStyle get orderCardDuration;

  TextStyle get settingsNameStyle;

  TextStyle get settingsMemberSinceStyle;

  TextStyle get settingsItemStyle;

  TextStyle get settingsOnOffStyle;

  TextStyle get searchTextStyle;

  TextStyle get notificationTextStyle;

  TextStyle get notificationDateStyle;

  TextStyle get orderStatusTextStyle;

  TextStyle get orderIdStyle;

  TextStyle get orderDateStyle;

  TextStyle get orderDetailsTitleStyle;

  TextStyle get orderDetailsContentStyle;

  TextStyle get orderCardButtonTextStyle;
}
