import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/src/painting/text_style.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:security_company_app/app/common/consts/palette.dart';
import 'package:security_company_app/app/common/themes/sec_theme.dart';

class DefaultTheme extends SecTheme {
  @override
  // TODO: implement buttonTextStyle
  TextStyle get buttonTextStyle => null;

  @override
  TextStyle get headStyle => TextStyle(
        fontFamily: 'Rubik',
        fontSize: ScreenUtil().setSp(25).toDouble(),
        fontWeight: FontWeight.w700,
        color: Palette.ebonyClay,
      );

  @override
  TextStyle get hintStyle => TextStyle(
        fontFamily: 'Rubik',
        fontSize: ScreenUtil().setSp(14).toDouble(),
        color: Palette.frenchGray,
      );

  @override
  // TODO: implement link1Style
  TextStyle get link1Style => null;

  @override
  // TODO: implement link2Style
  TextStyle get link2Style => null;

  @override
  // TODO: implement logoFullPath
  String get logoFullPath => null;

  @override
  // TODO: implement logoShortPath
  String get logoShortPath => null;

  @override
  // TODO: implement primaryColor
  Color get primaryColor => null;

  @override
  // TODO: implement secondaryColor
  Color get secondaryColor => null;

  @override
  TextStyle get subheadStyle => TextStyle(
        fontFamily: 'Rubik',
        fontSize: ScreenUtil().setSp(12).toDouble(),
        color: Palette.frenchGray,
      );

  @override
  // TODO: implement subtitleStyle
  TextStyle get subtitleStyle => null;

  @override
  // TODO: implement textFieldStyle
  TextStyle get textFieldStyle => null;

  @override
  TextStyle get titleStyle => TextStyle(
        fontFamily: 'Rubik',
        fontSize: ScreenUtil().setSp(27).toDouble(),
        fontWeight: FontWeight.w700,
        color: Palette.springWood,
      );

  @override
  TextStyle get introHeadStyle => TextStyle(
        fontFamily: 'Rubik',
        fontSize: ScreenUtil().setSp(25).toDouble(),
        fontWeight: FontWeight.w700,
        color: Colors.white,
      );

  @override
  TextStyle get introTextStyle => TextStyle(
        fontFamily: 'Rubik',
        fontSize: ScreenUtil().setSp(15).toDouble(),
        color: Colors.white,
      );

  @override
  TextStyle get introButtonTextStyle => TextStyle(
        fontFamily: 'Rubik',
        fontSize: ScreenUtil().setSp(18).toDouble(),
        fontWeight: FontWeight.w700,
        color: Palette.sanMarino,
      );

  @override
  TextStyle get screenTitleStyle => TextStyle(
        fontFamily: 'Rubik',
        fontSize: ScreenUtil().setSp(17).toDouble(),
        fontWeight: FontWeight.w700,
        letterSpacing: 1,
        color: Palette.sanMarino,
      );

  @override
  TextStyle get screenSubtitleStyle => TextStyle(
        fontFamily: 'Rubik',
        fontSize: ScreenUtil().setSp(23).toDouble(),
        fontWeight: FontWeight.w500,
        color: Palette.brightGray,
      );

  @override
  TextStyle get bodyTextStyle => TextStyle(
        fontFamily: 'Rubik',
        fontSize: ScreenUtil().setSp(12).toDouble(),
        fontWeight: FontWeight.w300,
        color: Palette.waterloo,
      );

  @override
  TextStyle get serviceNameStyle => TextStyle(
        fontFamily: 'Rubik',
        fontSize: ScreenUtil().setSp(20).toDouble(),
        fontWeight: FontWeight.w700,
        letterSpacing: 1.33,
        color: Palette.sanMarino,
      );

  @override
  TextStyle get serviceDescriptionStyle => TextStyle(
        fontFamily: 'Rubik',
        fontSize: ScreenUtil().setSp(9).toDouble(),
        fontWeight: FontWeight.w300,
        letterSpacing: 0.6,
        color: Palette.sanMarino,
      );

  @override
  TextStyle get subServiceDescriptionStyle => TextStyle(
        fontFamily: 'Rubik',
        fontSize: ScreenUtil().setSp(9).toDouble(),
        fontWeight: FontWeight.w500,
        letterSpacing: 0.6,
        color: Palette.sanMarino,
      );

  @override
  TextStyle get subServiceNameStyle => TextStyle(
        fontFamily: 'Rubik',
        fontSize: ScreenUtil().setSp(17).toDouble(),
        fontWeight: FontWeight.w700,
        letterSpacing: 0.5,
        color: Palette.sanMarino,
      );

  @override
  TextStyle get selectedSubServiceDescriptionStyle => TextStyle(
        fontFamily: 'Rubik',
        fontSize: ScreenUtil().setSp(9).toDouble(),
        fontWeight: FontWeight.w500,
        letterSpacing: 0.6,
        color: Palette.athensGray,
      );

  @override
  TextStyle get selectedSubServiceNameStyle => TextStyle(
        fontFamily: 'Rubik',
        fontSize: ScreenUtil().setSp(17).toDouble(),
        fontWeight: FontWeight.w700,
        letterSpacing: 0.5,
        color: Palette.athensGray,
      );

  @override
  TextStyle get calendarDisabledDayStyle => TextStyle(
        fontFamily: 'Rubik',
        fontSize: ScreenUtil().setSp(15).toDouble(),
        fontWeight: FontWeight.w500,
        color: Palette.frenchGray,
      );

  @override
  TextStyle get calendarEnabledDayStyle => TextStyle(
        fontFamily: 'Rubik',
        fontSize: ScreenUtil().setSp(15).toDouble(),
        fontWeight: FontWeight.w500,
        color: Palette.ebonyClay,
      );

  @override
  TextStyle get calendarWeekDayStyle => TextStyle(
        fontFamily: 'Rubik',
        fontSize: ScreenUtil().setSp(11).toDouble(),
        fontWeight: FontWeight.w300,
        color: Palette.frenchGray,
      );

  @override
  TextStyle get calenderSelectedDayStyle => TextStyle(
        fontFamily: 'Rubik',
        fontSize: ScreenUtil().setSp(17).toDouble(),
        fontWeight: FontWeight.w500,
        color: Colors.white,
      );

  @override
  TextStyle get dateScreenDateStyle => TextStyle(
        fontFamily: 'Rubik',
        fontSize: ScreenUtil().setSp(15).toDouble(),
        fontWeight: FontWeight.w500,
        color: Palette.brightGray,
      );

  @override
  TextStyle get dateScreenDayStyle => TextStyle(
        fontFamily: 'Rubik',
        fontSize: ScreenUtil().setSp(15).toDouble(),
        fontWeight: FontWeight.w300,
        color: Palette.brightGray,
      );

  @override
  TextStyle get timePickerTextStyle => TextStyle(
        fontFamily: 'Rubik',
        fontSize: ScreenUtil().setSp(27).toDouble(),
        fontWeight: FontWeight.w700,
        letterSpacing: 0,
        color: Palette.brightGray,
      );

  @override
  TextStyle get selectedGuardTypeStyle => TextStyle(
        fontFamily: 'Rubik',
        fontSize: ScreenUtil().setSp(17).toDouble(),
        fontWeight: FontWeight.w700,
        letterSpacing: 1,
        color: Palette.springWood,
      );

  @override
  TextStyle get guardTypeStyle => TextStyle(
        fontFamily: 'Rubik',
        fontSize: ScreenUtil().setSp(17).toDouble(),
        fontWeight: FontWeight.w700,
        letterSpacing: 1,
        color: Palette.sanMarino,
      );

  @override
  TextStyle get guardsNumberStyle => TextStyle(
        fontFamily: 'Rubik',
        fontSize: ScreenUtil().setSp(46).toDouble(),
        fontWeight: FontWeight.w500,
        color: Palette.brightGray,
      );

  @override
  TextStyle get companyNameStyle => TextStyle(
        fontFamily: 'Rubik',
        fontSize: ScreenUtil().setSp(17).toDouble(),
        fontWeight: FontWeight.w700,
        color: Palette.sanMarino,
      );

  @override
  TextStyle get distanceStyle => TextStyle(
        fontFamily: 'Rubik',
        fontSize: ScreenUtil().setSp(14).toDouble(),
        fontWeight: FontWeight.w500,
        color: Palette.periwinkleGray,
        letterSpacing: 0.8,
      );

  @override
  TextStyle get perDayStyle => TextStyle(
        fontFamily: 'Rubik',
        fontSize: ScreenUtil().setSp(17).toDouble(),
        fontWeight: FontWeight.w400,
        color: Palette.brightGray,
      );

  @override
  TextStyle get priceStyle => TextStyle(
        fontFamily: 'Rubik',
        fontSize: ScreenUtil().setSp(28).toDouble(),
        fontWeight: FontWeight.w700,
        color: Palette.brightGray,
      );

  @override
  TextStyle get resultDetailsContentStyle => TextStyle(
        fontFamily: 'Rubik',
        fontSize: ScreenUtil().setSp(12).toDouble(),
        fontWeight: FontWeight.w700,
        color: Palette.ebonyClay,
      );

  @override
  TextStyle get resultDetailsTitleStyle => TextStyle(
        fontFamily: 'Rubik',
        fontSize: ScreenUtil().setSp(12).toDouble(),
        fontWeight: FontWeight.w400,
        color: Palette.ebonyClay,
      );

  @override
  TextStyle get payNowStyle => TextStyle(
        fontFamily: 'Rubik',
        fontSize: ScreenUtil().setSp(21).toDouble(),
        fontWeight: FontWeight.w700,
        letterSpacing: -0.3,
        color: Palette.springWood,
      );

  @override
  TextStyle get payPriceStyle => TextStyle(
        fontFamily: 'Rubik',
        fontSize: ScreenUtil().setSp(18).toDouble(),
        fontWeight: FontWeight.w700,
        color: Palette.springWood,
      );

  @override
  TextStyle get totalOrderStyle => TextStyle(
        fontFamily: 'Rubik',
        fontSize: ScreenUtil().setSp(18).toDouble(),
        fontWeight: FontWeight.w400,
        color: Palette.springWood,
      );

  @override
  TextStyle get clientPaidSumStyle => TextStyle(
        fontFamily: 'Rubik',
        fontSize: ScreenUtil().setSp(30).toDouble(),
        fontWeight: FontWeight.w700,
        color: Palette.sanMarino,
      );

  @override
  TextStyle get clientTotalOrdersStyle => TextStyle(
        fontFamily: 'Rubik',
        fontSize: ScreenUtil().setSp(15).toDouble(),
        fontWeight: FontWeight.w500,
        color: Palette.sanMarino,
      );

  @override
  TextStyle get filterTextStyle => TextStyle(
        fontFamily: 'Rubik',
        fontSize: ScreenUtil().setSp(13.0).toDouble(),
        fontWeight: FontWeight.w500,
        color: Palette.sanMarino,
      );

  @override
  TextStyle get orderCardCompanyName => TextStyle(
        fontFamily: 'Rubik',
        fontSize: ScreenUtil().setSp(14).toDouble(),
        fontWeight: FontWeight.w400,
        color: Palette.spindle,
      );

  @override
  TextStyle get orderCardDate => TextStyle(
        fontFamily: 'Rubik',
        fontSize: ScreenUtil().setSp(11).toDouble(),
        fontWeight: FontWeight.w300,
        color: Palette.spindle,
      );

  @override
  TextStyle get orderCardDuration => TextStyle(
        fontFamily: 'Rubik',
        fontSize: ScreenUtil().setSp(11).toDouble(),
        fontWeight: FontWeight.w400,
        color: Palette.sanMarino,
      );

  @override
  TextStyle get orderCardPrice => TextStyle(
        fontFamily: 'Rubik',
        fontSize: ScreenUtil().setSp(17).toDouble(),
        fontWeight: FontWeight.w700,
        color: Palette.sanMarino,
      );

  @override
  TextStyle get orderCardServiceName => TextStyle(
        fontFamily: 'Rubik',
        fontSize: ScreenUtil().setSp(16).toDouble(),
        fontWeight: FontWeight.w700,
        color: Palette.sanMarino,
      );

  @override
  TextStyle get orderDateHead => TextStyle(
        fontFamily: 'Rubik',
        fontSize: ScreenUtil().setSp(16).toDouble(),
        fontWeight: FontWeight.w500,
        color: Palette.spindle,
      );

  @override
  TextStyle get settingsItemStyle => TextStyle(
        fontFamily: 'Rubik',
        fontSize: ScreenUtil().setSp(17).toDouble(),
        fontWeight: FontWeight.w400,
        color: Colors.black,
      );

  @override
  TextStyle get settingsMemberSinceStyle => TextStyle(
        fontFamily: 'Rubik',
        fontSize: ScreenUtil().setSp(11).toDouble(),
        fontWeight: FontWeight.w400,
        color: Palette.dustyGray,
      );

  @override
  TextStyle get settingsNameStyle => TextStyle(
        fontFamily: 'Rubik',
        fontSize: ScreenUtil().setSp(24).toDouble(),
        fontWeight: FontWeight.w700,
        color: Palette.sanMarino,
      );

  @override
  TextStyle get settingsOnOffStyle => TextStyle(
        fontFamily: 'Rubik',
        fontSize: ScreenUtil().setSp(17).toDouble(),
        fontWeight: FontWeight.w400,
        color: Palette.silverChalice,
      );

  @override
  TextStyle get searchTextStyle => TextStyle(
        fontFamily: 'Rubik',
        fontSize: ScreenUtil().setSp(15).toDouble(),
        fontWeight: FontWeight.w400,
        color: Palette.sanMarino,
      );

  @override
  TextStyle get notificationDateStyle => TextStyle(
        fontFamily: 'Rubik',
        fontSize: ScreenUtil().setSp(9).toDouble(),
        fontWeight: FontWeight.w300,
        color: Palette.luckyPoint,
      );

  @override
  TextStyle get notificationTextStyle => TextStyle(
        fontFamily: 'Rubik',
        fontSize: ScreenUtil().setSp(11).toDouble(),
        fontWeight: FontWeight.w500,
        color: Palette.luckyPoint,
      );

  @override
  TextStyle get screenSubtitleSmallStyle => TextStyle(
        fontFamily: 'Rubik',
        fontSize: ScreenUtil().setSp(13).toDouble(),
        fontWeight: FontWeight.w400,
        color: Palette.brightGray,
      );

  @override
  TextStyle get orderCardButtonTextStyle => TextStyle(
        fontFamily: 'Rubik',
        fontSize: ScreenUtil().setSp(17).toDouble(),
        fontWeight: FontWeight.w700,
        color: Palette.springWood,
      );

  @override
  TextStyle get orderDateStyle => TextStyle(
        fontFamily: 'Rubik',
        fontSize: ScreenUtil().setSp(15).toDouble(),
        fontWeight: FontWeight.w300,
        letterSpacing: 0.44,
        color: Palette.springWood,
      );

  @override
  TextStyle get orderDetailsContentStyle => TextStyle(
        fontFamily: 'Rubik',
        fontSize: ScreenUtil().setSp(12).toDouble(),
        fontWeight: FontWeight.w700,
        color: Colors.white,
      );

  @override
  TextStyle get orderDetailsTitleStyle => TextStyle(
        fontFamily: 'Rubik',
        fontSize: ScreenUtil().setSp(12).toDouble(),
        fontWeight: FontWeight.w400,
        color: Colors.white,
      );

  @override
  TextStyle get orderIdStyle => TextStyle(
        fontFamily: 'Rubik',
        fontSize: ScreenUtil().setSp(17).toDouble(),
        fontWeight: FontWeight.w300,
        letterSpacing: 0.5,
        color: Palette.springWood,
      );

  @override
  TextStyle get orderStatusTextStyle => TextStyle(
        fontFamily: 'Rubik',
        fontSize: ScreenUtil().setSp(17).toDouble(),
        fontWeight: FontWeight.w700,
        letterSpacing: 0.5,
        color: Palette.springWood,
      );
}
