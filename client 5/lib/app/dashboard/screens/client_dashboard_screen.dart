import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:security_company_app/app/apis/dashboard_services.dart';
import 'package:security_company_app/app/common/consts/palette.dart';
import 'package:security_company_app/app/common/helpers/theme_helpers.dart';
import 'package:security_company_app/app/common/models/order_status.dart';
import 'package:security_company_app/app/common/models/status.dart';
import 'package:security_company_app/app/common/widgets/avatar.dart';
import 'package:security_company_app/app/dashboard/widgets/order_card_details.dart';
import 'package:security_company_app/app/dashboard/widgets/sec_filter_chip.dart';
import 'package:security_company_app/app/models/dahsboard.dart';
import 'package:security_company_app/app/security_app/bloc/bloc.dart';

class ClientDashboardScreen extends StatefulWidget {
  ClientDashboardScreen({Key key, this.pageController}) : super(key: key);
  final PageController pageController;

  @override
  _ClientDashboardScreenState createState() => _ClientDashboardScreenState();
}

class _ClientDashboardScreenState extends State<ClientDashboardScreen> {
  String _statusFilter = null;
  List<Order> _orders;
  List<StatusOrder> _statusorders = [];

  bool loading = false;
  bool loading1 = false;

  double price = 0.0;

  void _navigateToPage(int index) {
    widget.pageController.animateToPage(index,
        duration: Duration(milliseconds: 100), curve: Curves.ease);
  }

  getStausOrder() async {
    setState(() {
      loading1 = true;
    });
    List res = (await DashboardServices.getStatus()) as List;
    if (!this.mounted) return;
    setState(() {
      _statusorders = List<StatusOrder>.from(res);
      _statusorders.insert(
          0,
          StatusOrder(
            order_statusid: "0",
            order_status: "All",
          ));
      _statusFilter = "All";

      loading1 = false;
    });
  }

  getOrder() async {
    setState(() {
      loading = true;
    });
    List res = (await DashboardServices.get_dashboard_infos()) as List;
    if (!this.mounted) return;
    setState(() {
      _orders = List<Order>.from(res);
      loading = false;
    });

    double or = 0.0;
    for (Order order in _orders) {
      or = or + double.parse(order.order_totalprice);
    }

    setState(() {
      price = or;
    });
  }

  @override
  void initState() {
    super.initState();
    getStausOrder();
    getOrder();

    /* _orders = [
      Order(
        id: 'SG0001',
        status: OrderStatus.done,
        company: Company(
          id: '1',
          name: 'ThunderSec',
          available: true,
          phoneNumber: '+4456543256',
        ),
        address: 'London, UK',
        dateCreated: DateTime(2020, 2, 27, 12, 30, 43),
        dateFrom: DateTime(2020, 2, 28, 8),
        dateTo: DateTime(2020, 3, 5),
        guardType: GuardType.securityGuard,
        guardsCount: 2,
        service: MannedSecuritySubService(),
        price: 120,
      ),
      Order(
        id: 'BG0003',
        status: OrderStatus.pending,
        bodyguard: Bodyguard(
          id: '5',
          name: 'Brian Norton',
          available: true,
          phoneNumber: '+4456543256',
        ),
        address: 'London, UK',
        dateCreated: DateTime(2020, 2, 28, 12, 30, 43),
        dateFrom: DateTime(2020, 2, 28, 8),
        dateTo: DateTime(2020, 3, 5),
        guardType: GuardType.bodyGuard,
        guardsCount: 1,
        service: StaticSecuritySubService(),
        price: 80,
      ),
      Order(
        id: 'SG0005',
        status: OrderStatus.pending,
        company: Company(
          id: '1',
          name: 'ThunderSec',
          available: true,
          phoneNumber: '+4456543256',
        ),
        address: 'London, UK',
        dateCreated: DateTime(2020, 2, 27, 12, 30, 43),
        dateFrom: DateTime(2020, 3, 2, 8),
        dateTo: DateTime(2020, 3, 5),
        guardType: GuardType.securityGuard,
        guardsCount: 2,
        service: DoorSupervisionSubService(),
        price: 120,
      ),
    ];*/
  }

  @override
  Widget build(BuildContext context) {
    Widget loadwid = Center(child: CupertinoActivityIndicator());
    ScreenUtil.init(context, width: 375, height: 812);

    return BlocBuilder<AppBloc, AppState>(
      builder: (context, appState) {
        final theme = getThemeFromState(appState);
        return Scaffold(
          body: Stack(
            fit: StackFit.expand,
            children: <Widget>[
              Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('assets/images/dashboard-background.png'),
                    fit: BoxFit.cover,
                    alignment: Alignment.topRight,
                  ),
                  color: Palette.mystic,
                ),
                child: SafeArea(
                  child: Column(
                    children: <Widget>[
                      SizedBox(height: ScreenUtil().setHeight(37).toDouble()),
                      Container(
                        margin: EdgeInsets.symmetric(
                          horizontal: ScreenUtil().setWidth(29).toDouble(),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  '£' + price.toString(),
                                  style: theme.clientPaidSumStyle,
                                ),
                                SizedBox(
                                    height:
                                        ScreenUtil().setHeight(7).toDouble()),
                                Text(
                                  'Total orders paid',
                                  style: theme.clientTotalOrdersStyle,
                                ),
                              ],
                            ),
                            InkWell(
                              onTap: () {
                                _navigateToPage(3);
                                //Router.navigator.pushNamed(Router.settingsScreen);
                              },
                              child: Avatar(
                                radius:
                                    ScreenUtil().setWidth(58).toDouble() / 2,
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: ScreenUtil().setHeight(31).toDouble()),
                      Expanded(
                        child: Container(
                          margin: EdgeInsets.symmetric(
                            horizontal: ScreenUtil().setWidth(8).toDouble(),
                          ),
                          padding: EdgeInsetsDirectional.only(
                            top: ScreenUtil().setHeight(20).toDouble(),
                            end: ScreenUtil().setWidth(9).toDouble(),
                            start: ScreenUtil().setWidth(9).toDouble(),
                          ),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(12),
                            color: Palette.mystic,
                          ),
                          child: Column(
                            children: <Widget>[
                              Builder(
                                builder: (context) {
                                  /*final statusFilters = [
                                    {
                                      'filter': null,
                                      'text': 'All',
                                    },
                                    {
                                      'filter': "pending",
                                      'text': 'Pending',
                                    },
                                    {
                                      'filter': "cancelled",
                                      'text': 'Canceled',
                                    },
                                    {
                                      'filter': "done",
                                      'text': 'Done',
                                    },
                                  ];*/

                                  return _statusorders.isEmpty
                                      ? CupertinoActivityIndicator()
                                      : Container(
                                          height: ScreenUtil()
                                              .setHeight(36)
                                              .toDouble(),
                                          child: ListView(
                                            scrollDirection: Axis.horizontal,
                                            children: <Widget>[
                                              for (var filterData
                                                  in _statusorders)
                                                Builder(
                                                  builder: (context) {
                                                    final filter = filterData
                                                        .order_status as String;
                                                    final text = filterData
                                                        .order_status as String;
                                                    final selected =
                                                        _statusFilter == filter;

                                                    return SecFilterChip(
                                                      onTap: () {
                                                        setState(() {
                                                          _statusFilter =
                                                              filter as String;
                                                        });
                                                      },
                                                      selected: selected,
                                                      child: Text(
                                                        text.toString(),
                                                        style: selected
                                                            ? theme
                                                                .filterTextStyle
                                                                .copyWith(
                                                                color: Colors
                                                                    .white,
                                                              )
                                                            : theme
                                                                .filterTextStyle,
                                                      ),
                                                    );
                                                  },
                                                ),
                                            ],
                                          ));
                                },
                              ),
                              SizedBox(
                                  height:
                                      ScreenUtil().setHeight(22).toDouble()),
                              loading
                                  ? Container(
                                      width: MediaQuery.of(context).size.width *
                                          0.8,
                                      child: loadwid)
                                  : _orders.isEmpty
                                      ? Center(
                                          child: Text(
                                            "No result found",
                                            style: theme.screenTitleStyle,
                                          ),
                                        )
                                      : Expanded(
                                          child: Builder(
                                            builder: (context) {
                                              final ordersByDate =
                                                  <DateTime, List<Order>>{};
                                              final ordersFiltered =
                                                  _orders.where((order) =>
                                                      _statusFilter == "All" ||
                                                      order.status ==
                                                          _statusFilter);
                                              ordersFiltered
                                                  .forEach((Order order) {
                                                final orderDate =
                                                    DateTime.parse(
                                                        order.date_order);
                                                final date = DateTime(
                                                    orderDate.year,
                                                    orderDate.month);

                                                if (!ordersByDate
                                                    .containsKey(date)) {
                                                  ordersByDate[date] = [];
                                                }

                                                ordersByDate[date].add(order);
                                              });

                                              final dates =
                                                  ordersByDate.keys.toList();
                                              dates.sort((date1, date2) =>
                                                  date1.compareTo(date2));

                                              return ListView.separated(
                                                padding: EdgeInsets.only(
                                                    bottom:
                                                        MediaQuery.of(context)
                                                                .size
                                                                .height *
                                                            0.14),
                                                itemCount: dates.length,
                                                separatorBuilder: (_, __) =>
                                                    SizedBox(
                                                        height: ScreenUtil()
                                                            .setHeight(13)
                                                            .toDouble()),
                                                itemBuilder:
                                                    (BuildContext context,
                                                        int index) {
                                                  final date = dates[index];
                                                  final orders =
                                                      ordersByDate[date];

                                                  return Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: <Widget>[
                                                      Container(
                                                        margin: EdgeInsets
                                                            .symmetric(
                                                          horizontal:
                                                              ScreenUtil()
                                                                  .setWidth(
                                                                      21 - 9)
                                                                  .toDouble(),
                                                        ),
                                                        child: Text(
                                                          DateFormat('MMM y')
                                                              .format(date)
                                                              .toUpperCase(),
                                                          style: theme
                                                              .orderDateHead,
                                                        ),
                                                      ),
                                                      SizedBox(
                                                          height: ScreenUtil()
                                                              .setHeight(7)
                                                              .toDouble()),
                                                      Container(
                                                        padding: EdgeInsets
                                                            .symmetric(
                                                          horizontal:
                                                              ScreenUtil()
                                                                  .setWidth(14)
                                                                  .toDouble(),
                                                        ),
                                                        decoration:
                                                            BoxDecoration(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(12),
                                                          color: Colors.white,
                                                          boxShadow: [
                                                            BoxShadow(
                                                              color: Colors
                                                                  .black
                                                                  .withOpacity(
                                                                      0.08),
                                                              offset:
                                                                  Offset(0, 2),
                                                              blurRadius: 4,
                                                            ),
                                                          ],
                                                        ),
                                                        child: Column(
                                                          children: <Widget>[
                                                            for (var i = 0;
                                                                i <
                                                                    orders
                                                                        .length;
                                                                i++)
                                                              Builder(
                                                                builder:
                                                                    (context) {
                                                                  final order =
                                                                      orders[i];

                                                                  return Column(
                                                                    children: <
                                                                        Widget>[
                                                                      InkWell(
                                                                          onTap:
                                                                              () {
                                                                            print("hdh");

                                                                            Navigator.push(context,
                                                                                new MaterialPageRoute(builder: (BuildContext context) {
                                                                              return new OrderDetails(order);
                                                                            }));
                                                                          },
                                                                          child:
                                                                              Container(
                                                                            padding:
                                                                                EdgeInsets.symmetric(
                                                                              horizontal: ScreenUtil().setWidth(6).toDouble(),
                                                                              vertical: ScreenUtil().setHeight(19).toDouble(),
                                                                            ),
                                                                            child:
                                                                                Row(
                                                                              children: <Widget>[
                                                                                Container(
                                                                                  width: ScreenUtil().setWidth(38).toDouble(),
                                                                                  height: ScreenUtil().setHeight(38).toDouble(),
                                                                                  child: FittedBox(
                                                                                    fit: BoxFit.contain,
                                                                                    child: Image.asset('assets/images/security-guard-icon.png'),
                                                                                  ),
                                                                                ),
                                                                                SizedBox(width: ScreenUtil().setWidth(21).toDouble()),
                                                                                Expanded(
                                                                                  child: Column(
                                                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                                                    mainAxisAlignment: MainAxisAlignment.center,
                                                                                    children: <Widget>[
                                                                                      Text(
                                                                                        order.order_service.toString(),
                                                                                        style: theme.orderCardServiceName,
                                                                                      ),
                                                                                      Text(
                                                                                        order.provider.toString(),
                                                                                        style: theme.orderCardCompanyName,
                                                                                      ),
                                                                                      SizedBox(height: ScreenUtil().setHeight(7).toDouble()),
                                                                                      Text(
                                                                                        DateFormat('EEEE d MMM y').format(DateTime.parse(order.date_order)),
                                                                                        style: theme.orderCardDate,
                                                                                      ),
                                                                                    ],
                                                                                  ),
                                                                                ),
                                                                                SizedBox(width: ScreenUtil().setWidth(21).toDouble()),
                                                                                Column(
                                                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                                                  children: <Widget>[
                                                                                    Text(
                                                                                      '£${order.order_totalprice.toString()}',
                                                                                      style: theme.orderCardPrice,
                                                                                    ),
                                                                                    SizedBox(height: 1),
                                                                                    Builder(
                                                                                      builder: (context) {
                                                                                        final duration = DateTime.parse(order.order_end_time).difference(DateTime.parse(order.order_start_time)).inDays.toString() + " Days";

                                                                                        return Text(
                                                                                          duration.toString(),
                                                                                          style: theme.orderCardDuration,
                                                                                        );
                                                                                      },
                                                                                    ),
                                                                                    /* Text(//.difference(DateTime.parse(order. order_end_time)
                                                                              DateTime.parse(order.order_start_time).toString(),
                                                                              style: theme.orderCardDuration,
                                                                            )*/
                                                                                  ],
                                                                                ),
                                                                              ],
                                                                            ),
                                                                          )),
                                                                      if (i <
                                                                          orders.length -
                                                                              1)
                                                                        Container(
                                                                          height:
                                                                              0.2,
                                                                          color:
                                                                              Palette.silverChalice,
                                                                        ),
                                                                    ],
                                                                  );
                                                                },
                                                              ),
                                                          ],
                                                        ),
                                                      ),
                                                    ],
                                                  );
                                                },
                                              );
                                            },
                                          ),
                                        ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              //
              //
            ],
          ),
        );
      },
    );
  }
}
