import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:security_company_app/app/common/consts/palette.dart';
import 'package:security_company_app/app/common/helpers/theme_helpers.dart';
import 'package:security_company_app/app/common/widgets/sec_back_button.dart';
import 'package:security_company_app/app/common/widgets/sec_screen_subtitle.dart';
import 'package:security_company_app/app/dashboard/widgets/rate_widget.dart';
import 'package:security_company_app/app/models/dahsboard.dart';
import 'package:security_company_app/app/security_app/bloc/app_bloc.dart';
import 'package:security_company_app/app/security_app/bloc/app_state.dart';

class OrderDetails extends StatelessWidget {
  OrderDetails(this.order);

  Order order;
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();


  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AppBloc, AppState>(builder: (context, appState) {
      final theme = getThemeFromState(appState);

      Widget widget1 = Padding(
        padding: EdgeInsets.symmetric(
          horizontal: ScreenUtil().setWidth(30).toDouble(),
        ),
        child: Column(
          children: <Widget>[
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Text(
                    'Order ID',
                    style: theme.resultDetailsTitleStyle,
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Text(
                    order.preorderid,
                    style: theme.resultDetailsContentStyle,
                  ),
                ),
              ],
            ),
            SizedBox(height: ScreenUtil().setHeight(7).toDouble()),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Text(
                    'Order type',
                    style: theme.resultDetailsTitleStyle,
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Text(
                    order.order_state,
                    style: theme.resultDetailsContentStyle,
                  ),
                ),
              ],
            ),
            SizedBox(height: ScreenUtil().setHeight(7).toDouble()),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Text(
                    'Order mission',
                    style: theme.resultDetailsTitleStyle,
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Text(
                    order.order_service,
                    style: theme.resultDetailsContentStyle,
                  ),
                ),
              ],
            ),
            SizedBox(height: ScreenUtil().setHeight(7).toDouble()),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Text(
                    'Nº of Guards',
                    style: theme.resultDetailsTitleStyle,
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Text(
                    order.order_quantity,
                    style: theme.resultDetailsContentStyle,
                  ),
                ),
              ],
            ),
            SizedBox(height: ScreenUtil().setHeight(7).toDouble()),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Text(
                    'From',
                    style: theme.resultDetailsTitleStyle,
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Text(
                   DateFormat(
    'dd E y')
        .format(DateTime.parse(order
        .order_start_time
        ))
                    // '17 Jan 2020',
                    ,
                    style: theme.resultDetailsContentStyle,
                  ),
                ),
              ],
            ),
            SizedBox(height: ScreenUtil().setHeight(7).toDouble()),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Text(
                    'To',
                    style: theme.resultDetailsTitleStyle,
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Text(
                    DateFormat(
    'dd E y')
        .format(DateTime.parse(order
                        .order_end_time
                    ))
                    ,
                    style: theme.resultDetailsContentStyle,
                  ),
                ),
              ],
            ),
            SizedBox(height: ScreenUtil().setHeight(7).toDouble()),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Text(
                    'Address',
                    style: theme.resultDetailsTitleStyle,
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Text(
                    order.order_address1,
                    style: theme.resultDetailsContentStyle,
                  ),
                ),
              ],
            ),
          ],
        ),
      );



      Widget header =  Container(
        padding:
        EdgeInsets
            .symmetric(
          horizontal: ScreenUtil()
              .setWidth(
              30)
              .toDouble(),
          vertical: ScreenUtil()
              .setHeight(
              20)
              .toDouble(),
        ),
        child: Row(
          children: <
              Widget>[
            Container(
              width: ScreenUtil()
                  .setWidth(38)
                  .toDouble(),
              height: ScreenUtil()
                  .setHeight(38)
                  .toDouble(),
              child:
              FittedBox(
                fit:
                BoxFit.contain,
                child:
                Image.asset('assets/images/security-guard-icon.png'),
              ),
            ),
            SizedBox(
                width:
                ScreenUtil().setWidth(21).toDouble()),
            Expanded(
              child:
              Column(
                crossAxisAlignment:
                CrossAxisAlignment.start,
                mainAxisAlignment:
                MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    order.order_service.toString(),
                    style: theme.orderCardServiceName,
                  ),
                  Text(
                    order.provider.toString(),
                    style: theme.orderCardCompanyName,
                  ),
                  SizedBox(height: ScreenUtil().setHeight(7).toDouble()),
                  Text(
                    DateFormat('EEEE d MMM y').format(DateTime.parse(order.date_order)),
                    style: theme.orderCardDate,
                  ),
                ],
              ),
            ),
            SizedBox(
                width:
                ScreenUtil().setWidth(21).toDouble()),
            Column(
              mainAxisAlignment:
              MainAxisAlignment.center,
              children: <
                  Widget>[
                Text(
                  '£${order.order_totalprice.toString()}',
                  style: theme.orderCardPrice,
                ),
                SizedBox(height: 1),
                Builder(
                  builder:
                      (context) {
                    final duration = DateTime.parse(order
                        .order_end_time)
                        .difference(
                        DateTime.parse(order.order_start_time))
                        .inDays.toString() + " Days";

                    return Text(
                      duration
                          .toString(),
                      style: theme
                          .orderCardDuration,
                    );
                  },
                ),
                /* Text(//.difference(DateTime.parse(order. order_end_time)
                                                                              DateTime.parse(order.order_start_time).toString(),
                                                                              style: theme.orderCardDuration,
                                                                            )*/
              ],
            ),
          ],
        ),
      );

      Widget footer = InkWell(
        onTap: () {
          showDialog(context: context,builder: (_) => RateWidget(order.preorderid,_scaffoldKey));

        },
        child: Container(
          decoration: BoxDecoration(
            color: Palette.sanMarino,
          ),
          padding: EdgeInsetsDirectional.only(
            top: ScreenUtil().setHeight(22).toDouble(),
            end: ScreenUtil().setWidth(21).toDouble(),
            bottom: ScreenUtil().setHeight(20).toDouble(),
            start: ScreenUtil().setWidth(21).toDouble(),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[

              Text(
                'Rate us',
                style: theme.payNowStyle,
              ),
            ],
          ),
        ),
      );

      return Scaffold(
        key: _scaffoldKey,
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/images/background.png'),
              fit: BoxFit.cover,
              alignment: Alignment.topRight,
            ),
            color: Palette.mystic,
          ),
          child: SafeArea(
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                SizedBox(height: ScreenUtil().setHeight(8).toDouble()),
                Container(
                  margin: EdgeInsets.symmetric(
                    horizontal: ScreenUtil().setWidth(15).toDouble(),
                  ),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: Align(
                          alignment: AlignmentDirectional.centerStart,
                          child: SecBackButton(),
                        ),
                      ),
                      Expanded(
                        flex: 2,
                        child: Center(
                          child: Text(
                            'ORDER DETAILS',
                            style: theme.screenTitleStyle,
                          ),
                        ),
                      ),
                      Expanded(
                        child: Align(
                          alignment: AlignmentDirectional.centerEnd,
                          child: Container(),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: ScreenUtil().setHeight(48).toDouble()),


                ///jijiiiiiii
                InkWell(
                  onTap: () {},
                  child: Container(
                    margin: EdgeInsets.symmetric(
                      horizontal: ScreenUtil().setWidth(8).toDouble(),
                    ),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(12),
                      color: Palette.springWood,
                      boxShadow: [
                        BoxShadow(
                          color: Palette.sanMarino.withOpacity(0.22),
                          offset: Offset(1, 1),
                          blurRadius: 3,
                          spreadRadius: -1,
                        ),
                      ],
                    ),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(12),
                      child: Column(children: <Widget>[
                        Container(
                          margin: EdgeInsetsDirectional.only(
                            top: ScreenUtil().setHeight(16).toDouble(),
                            end: ScreenUtil().setWidth(32).toDouble(),
                            bottom: ScreenUtil().setHeight(13).toDouble(),
                            start: ScreenUtil().setWidth(24).toDouble(),
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[],
                          ),
                        ),
                        Column(
                          children: <Widget>[

                            header,

                           Container(
                            padding: EdgeInsets
                                 .symmetric(
                                 horizontal: ScreenUtil()
                                     .setWidth(
                                     30)
                                     .toDouble()),

                             child: Divider(height: 1.0)),
                            SizedBox(
                                height: ScreenUtil().setHeight(24).toDouble()),
                            widget1,
                            SizedBox(
                                height: ScreenUtil().setHeight(42).toDouble()),
                            footer
                          ],
                        ),
                      ]),
                    ),
                  ),
                ),

                ///jiji
              ])),
        ),
      );
    });
  }
}
