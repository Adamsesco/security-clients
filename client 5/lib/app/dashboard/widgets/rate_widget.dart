import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:security_company_app/app/apis/rest_services.dart';
import 'package:security_company_app/app/common/consts/palette.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';

class RateWidget extends StatefulWidget {
  RateWidget(this.order_id, this._scaffoldKey);

  String order_id;
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  _RateWidgetState createState() => _RateWidgetState();
}

class _RateWidgetState extends State<RateWidget> {
  RestService rest = new RestService();

  postRate() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String id = prefs.getString("id");
    var pr_resp = await rest.post(
        'makeRate',
        json.encode({
          "orderid": widget.order_id,
          "userid": id,
          "rate": rate,
          "reason_id": 1
        }));

    print(pr_resp);
  }

  double screentUtilStp(double val) {
    return ScreenUtil().setSp(val).toDouble();
  }

  double rate = 0.0;
  String msg = "";

  func(double v) {
    setState(() {
      rate = v;
      msg = rte();
    });
  }

 String rte() {
    String rt = rate.toString();

    switch (rt) {
      case "1.0":
        return "POOR";
        break;
      case "2.0":
        return "Sufficient".toUpperCase();
        break;
      case "3.0":
        return "Average".toUpperCase();
        break;
      case "4.0":
        return "Well".toUpperCase();
        break;
      case "5.0":
        return "Very Good".toUpperCase();
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          SizedBox(height: screentUtilStp(35)),
          Text(
            'RATE US',
            style: TextStyle(
              fontSize: screentUtilStp(17),
              fontWeight: FontWeight.bold,
              color: Color(0xff4361BA),
            ),
          ),
          SizedBox(height: screentUtilStp(45)),
          Text(
            msg,
            style: TextStyle(
              fontSize: screentUtilStp(29),
              fontWeight: FontWeight.bold,
              color: Color(0xff4361BA),
            ),
          ),
          SizedBox(height: screentUtilStp(10)),
          RatingRow(rate, func),
          SizedBox(height: screentUtilStp(45)),
          GestureDetector(
              onTap: () {
                print(rate);
                postRate();
                Navigator.pop(context);

                widget._scaffoldKey.currentState.showSnackBar(
                  SnackBar(
                      content: Text(
                        'Thank you for rating',
                        style: TextStyle(color: Colors.white, fontSize: 18),
                      ),
                      backgroundColor: Palette.cello),
                );
              },
              child: Container(
                height: screentUtilStp(65),
                decoration: BoxDecoration(
                  color: Color(0xff3E5CB5),
                  borderRadius:
                      BorderRadius.vertical(bottom: Radius.circular(20)),
                ),
                child: Center(
                  child: Text(
                    'Rate now',
                    style: TextStyle(
                      fontSize: screentUtilStp(21),
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
                  ),
                ),
              )),
        ],
      ),
    );
  }
}

class RatingRow extends StatefulWidget {
  RatingRow(
    this.rate,
    this.func, {
    Key key,
  }) : super(key: key);

  final func;
  double rate = 0.0;

  @override
  _RatingRowState createState() => _RatingRowState();
}

class _RatingRowState extends State<RatingRow> {
  double screentUtilStp(double val) {
    return ScreenUtil().setSp(val).toDouble();
  }

  String selectedItem = 'Not pleased';

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        SmoothStarRating(
          allowHalfRating: false,
          borderColor: Color(0xffC2D1E3),
          onRated: (v) {
            widget.func(v);
          },
          starCount: 5,
          size: 40.0,
          color: Color(0xff4361BA),
          filledIconData: Icons.star,
          rating: widget.rate,
          spacing: 0.0,
        ),
        widget.rate > 1 || widget.rate == 0
            ? SizedBox()
            : Container(
                margin: EdgeInsets.only(top: screentUtilStp(35)),
                padding: EdgeInsets.symmetric(horizontal: screentUtilStp(8)),
                width: ScreenUtil().setSp(230).toDouble(),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(7),
                  color: Color(0xffE4E9EF),
                ),
                child: DropdownButton(
                    icon: Row(
                      children: <Widget>[
                        Container(
                          height: screentUtilStp(30),
                          width: 1,
                          color: Color(0xff4361BA),
                        ),
                        Icon(
                          Icons.arrow_drop_down,
                          color: Color(0xff4361BA),
                          size: screentUtilStp(33),
                        )
                      ],
                    ),
                    isExpanded: true,
                    value: selectedItem,
                    onChanged: (val) {},
                    underline: SizedBox(),
                    items: ['Not pleased'].map((e) {
                      return DropdownMenuItem(
                        child: Text(
                          e.toUpperCase(),
                          style: TextStyle(
                            fontSize: screentUtilStp(13),
                            color: Color(0xff4361BA),
                          ),
                        ),
                        value: e,
                      );
                    }).toList()),
              ),
      ],
    );
  }
}
