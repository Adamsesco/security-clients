import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:security_company_app/app/common/consts/palette.dart';
import 'package:security_company_app/app/common/helpers/theme_helpers.dart';
import 'package:security_company_app/app/common/models/order_status.dart';
import 'package:security_company_app/app/common/widgets/expandible_container.dart';
import 'package:security_company_app/app/models/dahsboard.dart';
import 'package:security_company_app/app/security_app/bloc/bloc.dart';

class OrderCard extends StatelessWidget {
  final Order order;
  final bool expanded;
  final void Function() onTap;
  final void Function(Order order) onAction;

  OrderCard(
      {@required this.order, this.expanded = false, this.onTap, this.onAction})
      : assert(order != null);

  @override
  Widget build(BuildContext context) {
    Color cardColor;
    Color buttonColor;
    String statusText;

    switch (order.order_state) {
      case "":
        cardColor = Palette.sanMarino;
        buttonColor = Palette.azure;
        statusText = 'NEW Order';
        break;
      case "pending":
        cardColor = Palette.treePoppy;
        buttonColor = Palette.sunshade;
        statusText = 'Pending';
        break;
      case "progress":
        cardColor = Palette.greenHaze;
        buttonColor = Palette.greenHaze2;
        statusText = 'In Progress';
        break;
      case 'done':
        cardColor = Colors.white;
        buttonColor = Palette.gallery;
        statusText = 'Completed';
        break;
      case "delayed":
        // TODO: Handle this case.
        break;
      case 'canceled':
        // TODO: Handle this case.
        break;
    }

    return BlocBuilder<AppBloc, AppState>(
      builder: (context, appState) {
        final theme = getThemeFromState(appState);

        return ClipRRect(
          borderRadius: BorderRadius.circular(12),
          child: InkWell(
            onTap: onTap,
            child: Container(
              padding: EdgeInsets.symmetric(
                horizontal: ScreenUtil().setWidth(14).toDouble(),
              ),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(12),
                color: cardColor,
              ),
              child: Column(
                children: <Widget>[
                  SizedBox(height: ScreenUtil().setHeight(16).toDouble()),
                  Row(
                    children: <Widget>[
                      RichText(
                        text: TextSpan(
                          style: theme.orderStatusTextStyle,
                          text: '$statusText  ',
                          children: [
                            TextSpan(
                              style: theme.orderIdStyle,
                              text: order.preorderid,
                            ),
                          ],
                        ),
                      ),
                      Spacer(),
                      Text(
                        DateFormat('dd/MM/y').format(DateTime.parse(order.date_order)),
                        style: theme.orderDateStyle,
                      ),
                    ],
                  ),
                  SizedBox(height: ScreenUtil().setHeight(14).toDouble()),
                  Visibility(
                    visible: !expanded,
                    maintainSize: true,
                    maintainState: true,
                    maintainAnimation: true,
                    child: Container(
                      width: ScreenUtil().setWidth(56.67).toDouble(),
                      height: ScreenUtil().setHeight(5).toDouble(),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(4),
                        color: Colors.white.withOpacity(0.44),
                      ),
                    ),
                  ),
                  SizedBox(height: ScreenUtil().setHeight(6).toDouble()),
                  ExpandibleContainer(
                    expanded: expanded,
                    curve: Curves.ease,
                    duration: const Duration(milliseconds: 400),
                    child: Column(
                      children: <Widget>[
                        Container(
                          margin: EdgeInsetsDirectional.only(
//                            top: ScreenUtil().setHeight(3).toDouble(),
                            end: ScreenUtil().setWidth(16).toDouble(),
                            bottom: ScreenUtil().setHeight(27).toDouble(),
                            start: ScreenUtil().setWidth(16).toDouble(),
                          ),
                          child: Column(
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Expanded(
                                    flex: 1,
                                    child: Text(
                                      'Order type',
                                      style: theme.orderDetailsTitleStyle,
                                    ),
                                  ),
                                  Expanded(
                                    flex: 2,
                                    child: Text(
                                      'Order type',
                                      style: theme.orderDetailsContentStyle,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: ScreenUtil().setHeight(4).toDouble()),
                              Row(
                                children: <Widget>[
                                  Expanded(
                                    flex: 1,
                                    child: Text(
                                      'Order mission',
                                      style: theme.orderDetailsTitleStyle,
                                    ),
                                  ),
                                  Expanded(
                                    flex: 2,
                                    child: Text(
                                      'Order type',
                                      style: theme.orderDetailsContentStyle,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: ScreenUtil().setHeight(4).toDouble()),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
