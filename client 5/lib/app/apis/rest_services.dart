import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
//import 'package:shared_preferences/shared_preferences.dart';

class RestService {
  final JsonDecoder _decoder = new JsonDecoder();
  String url = 'https://getsecured.app/backend/api/';




 get(String url2) async {
    var con = true;
    var results;
    if (con) {
      var url1 = Uri.parse(Uri.encodeFull(url + url2));

      var response = await http.get(url1);

      print(url1);

      String jsonBody = response.body;

      var statusCode = response.statusCode;
      if (statusCode < 200 || jsonBody == null || statusCode >= 300) {

        results = {'status' : 'error'};
      } else {
        results = {'status':'success', 'res':_decoder.convert(jsonBody)};
      }
    } else {
      results = {'status' : 'No Internet'};

    }
    return results;
  }
//headers: {"content-type": "application/json"},
  post(String url2, data) async {
    var con = true;
    var results;
    if (con) {
      var url1 = Uri.parse(Uri.encodeFull(url + url2));
      var response = await http.post(url1,
           body: data);
      String jsonBody = response.body;


      var statusCode = response.statusCode;
      if (statusCode < 200 || jsonBody == null || statusCode >= 300) {
        results = "error";
      } else {
        var postsContainer = _decoder.convert(jsonBody);
        results = postsContainer;
      }
    } else {
      results = "No Internet";
    }
    return results;
  }



  delete(String url2) async {
    var con = true;
    var results;
    if (con) {
      var url1 = Uri.parse(Uri.encodeFull(url + url2));
      var response = await http.delete(url1);
      String jsonBody = response.body;
      var statusCode = response.statusCode;
      if (statusCode < 200 || jsonBody == null || statusCode >= 300) {
        results = "error";
      } else {
        var postsContainer = _decoder.convert(jsonBody);
        results = postsContainer.length == 0;
      }
    } else {
      results = "No Internet";
    }
    return results;
  }
}
