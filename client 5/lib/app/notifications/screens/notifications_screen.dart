import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:security_company_app/app/apis/dashboard_services.dart';
import 'package:security_company_app/app/common/consts/palette.dart';
import 'package:security_company_app/app/common/helpers/theme_helpers.dart';
import 'package:security_company_app/app/common/models/notification.dart';
import 'package:security_company_app/app/notifications/widgets/notification_card.dart';
import 'package:security_company_app/app/notifications/widgets/search_text_field.dart';
import 'package:security_company_app/app/security_app/bloc/bloc.dart';

class NotificationsScreen extends StatefulWidget {
  NotificationsScreen({Key key}) : super(key: key);

  @override
  _NotificationsScreenState createState() => _NotificationsScreenState();
}

class _NotificationsScreenState extends State<NotificationsScreen> {
  List<SecNotification> _notifications;
  String _searchText;
  bool loading = false;


  getNotifications() async {
    setState(() {
      loading = true;
    });
    List res = (await DashboardServices.getNotificationsList()) as List;
    if (!this.mounted) return;
    setState(() {
      print(res);
      _notifications = List<SecNotification>.from(res);
      loading = false;
    });

  }

  @override
  void initState() {
    super.initState();
    getNotifications();

   /* _notifications = [
      SecNotification(
        id: '1',
        type: SecNotificationType.orderNew,
        text: 'You have a new order',
        date: DateTime.now(),
      ),
      SecNotification(
        id: '2',
        type: SecNotificationType.orderDone,
        text: 'Order completed',
        date: DateTime.now(),
        read: true,
      ),
    ];*/
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AppBloc, AppState>(builder: (context, appState) {
      final theme = getThemeFromState(appState);
      Widget loadwid = Center(child: CupertinoActivityIndicator());
      ScreenUtil.init(context, width: 375, height: 812);

      final notifications = _notifications?.where((notification) {
            final regexp = _searchText == null || _searchText.isEmpty
                ? RegExp('')
                : RegExp('.*$_searchText.*', caseSensitive: false);
            return regexp.hasMatch(notification.text);
          })?.toList() ??
          [];

      return Scaffold(
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/images/background.png'),
              fit: BoxFit.cover,
              alignment: Alignment.topRight,
            ),
            color: Palette.mystic,
          ),
          child: SafeArea(
            child: Column(
              children: <Widget>[
                SizedBox(height: ScreenUtil().setHeight(26).toDouble()),
                Container(
                  alignment: AlignmentDirectional.topEnd,
                  margin: EdgeInsetsDirectional.only(
                    end: ScreenUtil().setWidth(21).toDouble(),
                  ),
                  child: Text(
                    'NOTIFICATIONS  ',
                    style: theme.screenTitleStyle,
                  ),
                ),
                SizedBox(height: ScreenUtil().setHeight(10).toDouble()),
                SearchTextField(
                  onChanged: (String value) {
                    setState(() {
                      _searchText = value;
                    });
                  },
                ),
                SizedBox(height: ScreenUtil().setHeight(7).toDouble()),
                Align(
                  alignment: AlignmentDirectional.topEnd,
                  child: InkWell(
                    onTap: () {
                      for (var notification in _notifications) {
                        notification.read = true;
                      }
                      setState(() {});
                    },
                    child: Padding(
                      padding: EdgeInsets.symmetric(
                        vertical: ScreenUtil().setHeight(10).toDouble(),
                        horizontal: ScreenUtil().setWidth(21).toDouble(),
                      ),
                      child: Text(
                        'Mark all as read',
                        style: theme.searchTextStyle,
                      ),
                    ),
                  ),
                ),
                SizedBox(height: ScreenUtil().setHeight(10).toDouble()),
                loading
                    ? Container(
                    width: MediaQuery.of(context).size.width *
                        0.8,
                    child: loadwid)
                    : _notifications.isEmpty?Center(child: Text("No notification found",
                style: theme.screenTitleStyle,),) :Expanded(
                  child: ListView.separated(
                    itemCount: notifications.length,
                    separatorBuilder: (_, __) =>
                        SizedBox(height: ScreenUtil().setHeight(10).toDouble()),
                    itemBuilder: (BuildContext context, int index) {
                      final notification = notifications[index];

                      return Container(
                        margin: EdgeInsets.symmetric(
                          horizontal: ScreenUtil().setWidth(12).toDouble(),
                        ),
                        child: NotificationCard(notification: notification),
                      );
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    });
  }
}
