import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:security_company_app/app/settings/screens/new_password_screen.dart';
import 'package:security_company_app/app/settings/screens/settings_screen.dart';
import 'package:security_company_app/app/settings/screens/update_lang_screen.dart';
import 'package:security_company_app/app/settings/screens/update_name_screen.dart';
import 'package:security_company_app/app/settings/screens/update_phone_screen.dart';

class SettingsNavigator extends StatelessWidget {
  final PageController pageController;

  const SettingsNavigator({Key key, this.pageController}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Navigator(
      initialRoute: '/settings',
      onGenerateRoute: (settings){
        return CupertinoPageRoute(builder: (_){
          switch (settings.name) {
            case '/settings':
              return SettingsScreen(
                pageController: pageController,
              );
            case '/password':
            return NewPasswordScreen();
            case '/phone':
            return UpdatePhoneScreen();
            case '/language':
            return UpdateLangScreen();
            case '/name':
             return UpdateNameScreen();
            default:
            return SizedBox();
          }
        });
      },
    );
  }
}