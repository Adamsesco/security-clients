import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';
import 'package:security_company_app/app/common/widgets/sec_primary_button.dart';
import 'package:security_company_app/provider/profile_provider.dart';

const Color mainColor = Color(0xff212A37);

class NewPasswordScreen extends StatefulWidget {
  @override
  _NewPasswordScreenState createState() => _NewPasswordScreenState();
}

class _NewPasswordScreenState extends State<NewPasswordScreen> {
  double screenUtilStp(double number) {
    return ScreenUtil().setSp(number).toDouble();
  }


  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        decoration: BoxDecoration(
            color: const Color(0xffE3E9F0),

            image: DecorationImage(
          image: AssetImage('assets/images/background.png'),
          fit: BoxFit.cover,
          alignment: Alignment.topRight,
        )),
        child: SingleChildScrollView(
          padding: EdgeInsets.only(bottom: screenUtilStp(90)),
          child: Padding(
            padding: EdgeInsets.symmetric(
                horizontal: ScreenUtil().setSp(20).toDouble()),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  height: ScreenUtil().setSp(46).toDouble(),
                ),
                buildHeader(context),
                _buildTitleHeader(),
                _buildInputs(),
                SecPrimaryButton(
                  color: Color(0xFF4361BA),
                  text: 'Save',
                  isLoading: false,
                  onTap: () {
                    if(_formKey.currentState.validate()){
                      final profileProvider = Provider.of<ProfileProvider>(context,listen: false);
                      profileProvider.currentpassword = _currentPassController.text;
                      profileProvider.newPassword = _newPassController.text;
                    }
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  final TextEditingController _currentPassController = TextEditingController();

  final TextEditingController _newPassController = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  Widget _buildInputs() {
    return Container(
      margin:
          EdgeInsets.only(top: screenUtilStp(30), bottom: screenUtilStp(30)),
      padding: EdgeInsets.symmetric(
          vertical: screenUtilStp(40), horizontal: screenUtilStp(30)),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(screenUtilStp(20)),
      ),
      child: Form(
        key: _formKey,
        child: Column(
          children: <Widget>[
            CustomInputPassword(
              label: 'Current Password',
              controller: _currentPassController,
              validator: (String value) {
                if (value.isEmpty) {
                  return 'This field cannot be empty';
                }
                return null;
              },
            ),
            SizedBox(
              height: screenUtilStp(33),
            ),
            CustomInputPassword(
              label: 'New Password',
              controller: _newPassController,
              validator: (String value) {
                if (value.isEmpty) {
                  return 'This field cannot be empty';
                }
                if( _newPassController.text != _currentPassController.text ){
                  return 'Password not match';
                }
                return null;
              },
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildTitleHeader() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        SizedBox(
          height: screenUtilStp(50),
        ),
        Text(
          'Password update',
          style: TextStyle(
            fontSize: screenUtilStp(23),
            fontWeight: FontWeight.w500,
            color: Color(0xff363E49),
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 9),
          height: 3,
          width: 30,
          decoration: BoxDecoration(
            color: Color(0xff363E49),
            borderRadius: BorderRadius.circular(6),
          ),
        ),
        SizedBox(
          height: screenUtilStp(18),
        ),
        Text(
          'Please enter your current password first, in order to make a password change.',
          style:
              TextStyle(color: Color(0xff888792), fontSize: screenUtilStp(12)),
        ),
      ],
    );
  }

  Widget buildHeader(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        GestureDetector(
          onTap: () => Navigator.of(context).pop(),
          child: CircleAvatar(
            radius: ScreenUtil().setSp(27).toDouble(),
            backgroundColor: Colors.white,
            child: Icon(
              Icons.arrow_back,
              color: mainColor,
            ),
          ),
        ),
        Text(
          'SETTINGS',
          style: TextStyle(
              fontSize: ScreenUtil().setSp(17).toDouble(),
              color: Color(0xff4361BA),
              fontWeight: FontWeight.bold),
        )
      ],
    );
  }
}

typedef String Validator(String value);

class CustomInputPassword extends StatefulWidget {
  final String label;
  final TextEditingController controller;
  final Validator validator;
  CustomInputPassword({this.label, this.controller, this.validator});
  @override
  _CustomInputPasswordState createState() => _CustomInputPasswordState();
}

class _CustomInputPasswordState extends State<CustomInputPassword> {
  double screenUtilStp(double number) {
    return ScreenUtil().setSp(number).toDouble();
  }

  bool visibility = true;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      validator: widget.validator,
      controller: widget.controller,
      obscureText: visibility,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.only(
          bottom: ScreenUtil().setHeight(30.5).toDouble(),
        ),
        suffix: InkWell(
          onTap: () => setState(() => visibility = !visibility),
          child: Icon(
            visibility ? Icons.visibility_off : Icons.visibility,
            size: screenUtilStp(18),
          ),
        ),
        hintText: widget.label,
        hintStyle: TextStyle(
          fontFamily: 'Rubik',
          fontSize: ScreenUtil().setSp(14).toDouble(),
          color: Color(0xFFB8BCC7),
        ),
        border: UnderlineInputBorder(
          borderSide: BorderSide(
            color: Color(0xFFD8D8D8),
          ),
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: Color(0xFFD8D8D8),
          ),
        ),
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: Color(0xFFD8D8D8),
          ),
        ),
      ),
    );
  }
}
