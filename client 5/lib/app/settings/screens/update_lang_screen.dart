import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:security_company_app/app/common/widgets/sec_primary_button.dart';

const Color mainColor = Color(0xff212A37);

class UpdateLangScreen extends StatelessWidget {
  double screenUtilStp(double number) {
    return ScreenUtil().setSp(number).toDouble();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        decoration: BoxDecoration(
            image: DecorationImage(
          image: AssetImage('assets/images/background.png'),
          fit: BoxFit.cover,
          alignment: Alignment.topRight,
        )),
        child: SingleChildScrollView(
          padding: EdgeInsets.only(bottom: screenUtilStp(90)),
          child: Padding(
            padding: EdgeInsets.symmetric(
                horizontal: ScreenUtil().setSp(20).toDouble()),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  height: ScreenUtil().setSp(46).toDouble(),
                ),
                buildHeader(context),
                _buildTitleHeader(),
                _buildInputs(),
                SecPrimaryButton(
                  color: Color(0xFF4361BA),
                  text: 'Save',
                  isLoading: false,
                  onTap: () {},
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildInputs() {
    return Container(
      margin:
          EdgeInsets.only(top: screenUtilStp(30), bottom: screenUtilStp(30)),
      padding: EdgeInsets.symmetric(
          vertical: screenUtilStp(30), horizontal: screenUtilStp(30)),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(screenUtilStp(20)),
      ),
      child: SelectedLang(),
    );
  }

  Widget _buildTitleHeader() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        SizedBox(
          height: screenUtilStp(50),
        ),
        Text(
          'App Language',
          style: TextStyle(
            fontSize: screenUtilStp(23),
            fontWeight: FontWeight.w500,
            color: Color(0xff363E49),
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 9),
          height: 3,
          width: 30,
          decoration: BoxDecoration(
            color: Color(0xff363E49),
            borderRadius: BorderRadius.circular(6),
          ),
        ),
        SizedBox(
          height: screenUtilStp(18),
        ),
        Text(
          'Please choose the services that your company may be able to serve.',
          style:
              TextStyle(color: Color(0xff888792), fontSize: screenUtilStp(12)),
        ),
      ],
    );
  }

  Widget buildHeader(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        GestureDetector(
          onTap: () => Navigator.of(context).pop(),
          child: CircleAvatar(
            radius: ScreenUtil().setSp(27).toDouble(),
            backgroundColor: Colors.white,
            child: Icon(
              Icons.arrow_back,
              color: mainColor,
            ),
          ),
        ),
        Text(
          'SETTINGS',
          style: TextStyle(
              fontSize: ScreenUtil().setSp(17).toDouble(),
              color: Color(0xff4361BA),
              fontWeight: FontWeight.bold),
        )
      ],
    );
  }
}

class Langs {
  String label;
  int index;

  Langs({this.index, this.label});
}

class SelectedLang extends StatefulWidget {
  @override
  _SelectedLangState createState() => _SelectedLangState();
}

class _SelectedLangState extends State<SelectedLang> {
  final List<Langs> langs = [
    Langs(index: 0, label: 'English'),
    Langs(index: 1, label: 'العربية')
  ];

  double screenUtilStp(double number) {
    return ScreenUtil().setSp(number).toDouble();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: langs.map((lang) {
        return buildRow(lang);
      }).toList()
        ..insert(1, Divider()),
    );
  }

  int selectedIndex = 0;

  Widget buildRow(Langs lang) {
    return GestureDetector(
      onTap: ()=> setState(()=> selectedIndex = lang.index),
      child: Padding(
        padding: const EdgeInsets.only(bottom: 9),
        child: Container(
          width: double.infinity,
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                lang.label,
                style: TextStyle(
                  fontSize: screenUtilStp(17),
                  color: Colors.black,
                  fontWeight: FontWeight.w500,
                ),
              ),
              selectedIndex == lang.index
                  ? Icon(
                      Icons.check,
                      color: Color(0xff4361BA),
                    )
                  : SizedBox(),
            ],
          ),
        ),
      ),
    );
  }
}
