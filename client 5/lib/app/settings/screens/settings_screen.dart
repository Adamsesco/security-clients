import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';
import 'package:security_company_app/app/apis/dashboard_services.dart';
import 'package:security_company_app/app/common/consts/palette.dart';
import 'package:security_company_app/app/common/helpers/theme_helpers.dart';
import 'package:security_company_app/app/common/models/client.dart';
import 'package:security_company_app/app/common/widgets/avatar.dart';
import 'package:security_company_app/app/common/widgets/sec_navigation_bar.dart';
import 'package:security_company_app/app/common/widgets/sec_primary_button.dart';
import 'package:security_company_app/app/navigation.dart';
import 'package:security_company_app/app/routes/router.gr.dart';
import 'package:security_company_app/app/security_app/bloc/bloc.dart';
import 'package:security_company_app/app/settings/widgets/settings_item.dart';
import 'package:security_company_app/app/signin/login_provider.dart';
import 'package:security_company_app/app/signin/share_preferences.dart';
import 'package:security_company_app/provider/bottom_app_bar_provider.dart';
import 'package:security_company_app/provider/profile_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SettingsScreen extends StatefulWidget {
  final PageController pageController;

  SettingsScreen({Key key, @required this.pageController}) : super(key: key);

  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  bool _isAvailable = true;
  bool _notificationsEnabled = true;

  Client user = new Client(
      email: '',
      phoneNumber: '',
      name: '',
      firstName: '',
      lastName: '',
      avatarUrl: '');

  getUserInfo() async {
    Client client = (await DashboardServices.get_user_infos()) as Client;
    if (!this.mounted) return;
    setState(() {
      user = client;
      print(user.id);
      print(user.lastName);
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getUserInfo();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AppBloc, AppState>(builder: (context, appState) {
      final theme = getThemeFromState(appState);
      return Scaffold(
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/images/background.png'),
              fit: BoxFit.cover,
              alignment: Alignment.topRight,
            ),
            color: Palette.mystic,
          ),
          child: SafeArea(
            child: SingleChildScrollView(
              padding:
                  EdgeInsets.only(bottom: ScreenUtil().setSp(85).toDouble()),
              child: Column(
                children: <Widget>[
                  SizedBox(height: ScreenUtil().setHeight(26).toDouble()),
                  Container(
                    alignment: AlignmentDirectional.topEnd,
                    margin: EdgeInsetsDirectional.only(
                      end: ScreenUtil().setWidth(21).toDouble(),
                    ),
                    child: Text(
                      'SETTINGS',
                      style: theme.screenTitleStyle,
                    ),
                  ),
                  SizedBox(height: ScreenUtil().setHeight(15).toDouble()),
                  Container(
                    margin: EdgeInsets.symmetric(
                      horizontal: ScreenUtil().setWidth(8).toDouble(),
                    ),
                    padding: EdgeInsets.symmetric(
                      vertical: ScreenUtil().setHeight(17).toDouble(),
                    ),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(12),
                      image: DecorationImage(
                        image: AssetImage(
                            'assets/images/settings-header-background.png'),
                        fit: BoxFit.cover,
                      ),
                    ),
                    child: Row(
                      children: <Widget>[
                        SizedBox(width: ScreenUtil().setWidth(36).toDouble()),
                        Avatar(
                          radius: ScreenUtil().setWidth(66 / 2).toDouble(),
                        ),
                        SizedBox(width: ScreenUtil().setWidth(17).toDouble()),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              user?.lastName + " " + user?.firstName,
                              style: theme.settingsNameStyle,
                            ),
                            SizedBox(
                                height: ScreenUtil().setHeight(4).toDouble()),
                            Text(
                              'Member since ',
                              style: theme.settingsMemberSinceStyle,
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: ScreenUtil().setHeight(31).toDouble()),
                  Column(
                    children: <Widget>[
//                      SettingsItem(
//                        icon:
//                            Image.asset('assets/images/availability-icon.png'),
//                        text: 'Availability',
//                        hasSwitch: true,
//                        isActive: _isAvailable,
//                        onChanged: (bool value) {
//                          setState(() {
//                            _isAvailable = value;
//                          });
//                        },
//                      ),
//                      SizedBox(height: ScreenUtil().setHeight(21).toDouble()),
                      GestureDetector(
                        onTap: () {
                          Navigator.of(context).pushNamed('/name');
                        },
                        child: SettingsItem(
                          icon: Image.asset('assets/images/name-icon.png'),
                          text: 'Name',
                        ),
                      ),
                      SizedBox(height: ScreenUtil().setHeight(21).toDouble()),
                      GestureDetector(
                        onTap: () {
                          Navigator.of(context).pushNamed('/phone');
                        },
                        child: SettingsItem(
                          icon: Image.asset('assets/images/phone-icon.png'),
                          text: 'Phone',
                        ),
                      ),
                      SizedBox(height: ScreenUtil().setHeight(21).toDouble()),
                      GestureDetector(
                        onTap: () {
                          Navigator.of(context).pushNamed('/password');
                        },
                        child: SettingsItem(
                          icon: Image.asset('assets/images/password-icon.png'),
                          text: 'Password',
                        ),
                      ),
                      SizedBox(height: ScreenUtil().setHeight(30).toDouble()),
                      Container(
                        width: MediaQuery.of(context).size.width,
                        height: 0.2,
                        color: Palette.silverChalice,
                      ),
                      SizedBox(height: ScreenUtil().setHeight(30).toDouble()),
                      GestureDetector(
                        onTap: () =>
                            Navigator.of(context).pushNamed('/language'),
                        child: SettingsItem(
                          icon: Image.asset('assets/images/languages-icon.png'),
                          text: 'Languages',
                        ),
                      ),
                      SizedBox(height: ScreenUtil().setHeight(21).toDouble()),
                      InkWell(
                        onTap: () {
                          final bottomAppBarProvider =
                              Provider.of<BottomAppBarProvider>(context,
                                  listen: false);
                          bottomAppBarProvider.selectedIndex = 2;
                          widget.pageController.animateToPage(2,
                              duration: Duration(milliseconds: 300),
                              curve: Curves.ease);
                        },
                        child: SettingsItem(
                          icon: Image.asset(
                              'assets/images/notifications-icon.png'),
                          text: 'Notifications',
                          hasSwitch: true,
                          isActive: _notificationsEnabled,
                          onChanged: (bool value) {
                            setState(() {
                              _notificationsEnabled = value;
                            });
                          },
                        ),
                      ),
                      SizedBox(height: ScreenUtil().setHeight(30).toDouble()),
                      Container(
                        width: MediaQuery.of(context).size.width,
                        height: 0.2,
                        color: Palette.silverChalice,
                      ),
                      SizedBox(height: ScreenUtil().setHeight(30).toDouble()),
                      /**
                       *   Router.navigator.pushAndRemoveUntil(
                          CupertinoPageRoute(
                          builder: (_) => CustomNavigation(),
                          ),
                          (route) => false,
                          );
                       */
                      InkWell(
                          onTap: () async {
                            SharedPreferences prefs = await SharedPreferences.getInstance();
                            var myProvider = Provider.of<LoginProvider>(context,
                                listen: false);

                            if (myProvider.isAuthenticate) {
                              myProvider.mitexto = 'Provider';

                              myProvider.isAuthenticate = false;
                              //StorageUtil.putBool('isAutenticado', null);
                              prefs.clear();
                              /*Router.navigator.pushAndRemoveUntil(
                                MaterialPageRoute(
                                  builder: (_) => CustomNavigation(),
                                ),
                                (route) => false,
                              );*/
                            }
                            //Router.navigator.pushReplacementNamed(Router.mainScreen);
                          },
                          child: SettingsItem(
                            icon: Image.asset('assets/images/signout-icon.png'),
                            text: 'Sign out',
                            hasArrow: false,
                          )),
                      SizedBox(height: ScreenUtil().setHeight(33).toDouble()),
                      Consumer<ProfileProvider>(
                        builder: (_, provider, __) {
                          return Container(
                            margin: EdgeInsets.symmetric(
                              horizontal: ScreenUtil().setWidth(30).toDouble(),
                            ),
                            child: SecPrimaryButton(
                              isLoading: provider.isLoading,
                              text: 'Save changes',
                              onTap: () async {
                                final result = await provider.updateProfile();
                                if (result) {
                                  await Fluttertoast.showToast(
                                      msg: 'Updated successful');
                                } else {
                                  await Fluttertoast.showToast(
                                      msg: 'Updated failed');
                                }
                              },
                            ),
                          );
                        },
                      )
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      );
    });
  }
}
