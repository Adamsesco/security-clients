import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:security_company_app/app/common/consts/palette.dart';
import 'package:security_company_app/app/common/helpers/theme_helpers.dart';
import 'package:security_company_app/app/common/models/bodyguard_service.dart';
import 'package:security_company_app/app/common/models/event_security_service.dart';
import 'package:security_company_app/app/common/models/search_data.dart';
import 'package:security_company_app/app/common/models/sec_service.dart';
import 'package:security_company_app/app/common/models/security_guard_service.dart';
import 'package:security_company_app/app/common/widgets/sec_back_button.dart';
import 'package:security_company_app/app/common/widgets/sec_primary_button.dart';
import 'package:security_company_app/app/common/widgets/sec_screen_subtitle.dart';
import 'package:security_company_app/app/routes/router.gr.dart';
import 'package:security_company_app/app/security_app/bloc/bloc.dart';
import 'package:security_company_app/app/services/widgets/service_card.dart';
import 'package:security_company_app/app/services/widgets/sub_service_card.dart';
import 'package:flutter_svg/flutter_svg.dart';


class SelectServiceScreen extends StatefulWidget {
  final SearchData searchData;

  SelectServiceScreen({Key key, @required this.searchData}) : super(key: key);

  @override
  _SelectServiceScreenState createState() => _SelectServiceScreenState();
}

class _SelectServiceScreenState extends State<SelectServiceScreen> {
  List<SecService> _services;
  SecService _selectedService;
  SecService _selectedSubService;
  PageController _pageController;
  int _page = 0;

  Future<void> _getServices() async {
    final request =
        await Dio().get<String>('https://getsecured.app/backend/api/services');
    if (request == null ||
        request.statusCode != 200 ||
        request.data == null ||
        request.data.isEmpty) {
      setState(() {
        _services = [];
      });
      return;
    }

    final json = jsonDecode(request.data);
    final data = json['data'] as List<dynamic>;
    if (data == null || data.isEmpty) {
      setState(() {
        _services = [];
      });
      return;
    }

    setState(() {
      _services = data
          .map((serviceData) => SecService(
                id: serviceData['servicetypeid'] as String,
                name: serviceData['servicetype'] as String,
                icon: SvgPicture.network(serviceData['serviceimage'] as String),
                description: serviceData['serviceremarks'] as String,
                isVisible: serviceData['show_service'] == '1',
              ))
          .toList();
    });
  }

  @override
  void initState() {
    super.initState();

    _pageController = PageController();
    _getServices();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AppBloc, AppState>(
      builder: (context, appState) {
        final theme = getThemeFromState(appState);

        return Scaffold(
          body: Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/images/background.png'),
                fit: BoxFit.cover,
                alignment: Alignment.topRight,
              ),
              color: Palette.mystic,
            ),
            child: SafeArea(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(height: ScreenUtil().setHeight(8).toDouble()),
                  Container(
                    margin: EdgeInsets.symmetric(
                      horizontal: ScreenUtil().setWidth(15).toDouble(),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        SecBackButton(
                          onTap: () {
                            if (_page > 0) {
                              _pageController.previousPage(
                                  duration: Duration(milliseconds: 400),
                                  curve: Curves.ease);
                            } else {
                              Router.navigator.pop();
                            }
                          },
                        ),
                        Text(
                          'CHOOSE A SERVICE',
                          style: theme.screenTitleStyle,
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: ScreenUtil().setHeight(23).toDouble()),
                  SecScreenSubtitle(
                    subtitle: 'At vero eos censes',
                  ),
                  SizedBox(height: ScreenUtil().setHeight(15.32).toDouble()),
                  Container(
                    margin: EdgeInsets.symmetric(
                      horizontal: ScreenUtil().setWidth(15).toDouble(),
                    ),
                    child: Text(
                      'At vero eos censes aut odit aut quid percipit aut rerum necessitatibus saepe.',
                      style: theme.bodyTextStyle,
                    ),
                  ),
                  SizedBox(height: ScreenUtil().setHeight(16).toDouble()),
                  Expanded(
//                    height: ScreenUtil().setHeight(416).toDouble(),
                    child: PageView(
                      controller: _pageController,
                      onPageChanged: (int page) {
                        setState(() {
                          _page = page;
                          _selectedSubService = null;
                          if (page == 0) {
                            _selectedService = null;
                          }
                        });
                      },
                      children: <Widget>[
                        if (_services == null)
                          Center(
                            child: CupertinoActivityIndicator(),
                          ),
                        if (_services != null)
                          ListView.separated(
                            itemCount: _services.length,
                            separatorBuilder: (_, __) => SizedBox(
                                height: ScreenUtil().setHeight(10).toDouble()),
                            itemBuilder: (BuildContext context, int index) {
                              final service = _services[index];

                              return InkWell(
                                onTap: () {
                                  setState(() {
                                    _selectedService = service;
                                  });

                                  // _pageController.nextPage(
                                  //     duration: Duration(milliseconds: 400),
                                  //     curve: Curves.ease);
                                },
                                child: ServiceCard(
                                  index: index,
                                  service: _services[index],
                                  selected: _selectedService?.id == service.id ,
                                ),
                              );
                            },
                          ),
                        // if (_selectedService != null)
                        //   ListView.separated(
                        //     itemCount: _selectedService.subServices.length,
                        //     separatorBuilder: (_, __) => SizedBox(
                        //         height: ScreenUtil().setHeight(10).toDouble()),
                        //     itemBuilder: (BuildContext context, int index) {
                        //       final subService =
                        //           _selectedService.subServices[index];
                        //       return InkWell(
                        //         onTap: () {
                        //           setState(() {
                        //             _selectedSubService = subService;
                        //           });
                        //         },
                        //         child: SubServiceCard(
                        //           index: index,
                        //           service: subService,
                        //           selected: _selectedSubService?.name ==
                        //               subService.name,
                        //         ),
                        //       );
                        //     },
                        //   ),
                      ],
                    ),
                  ),
                  SizedBox(height: ScreenUtil().setHeight(24).toDouble()),
                  Visibility(
                    visible: true,
                    maintainSize: true,
                    maintainState: true,
                    maintainAnimation: true,
                    child: Container(
                      margin: EdgeInsets.symmetric(
                        horizontal: ScreenUtil().setWidth(30).toDouble(),
                      ),
                      child: SecPrimaryButton(
                        onTap: () {
                          widget.searchData.service = _selectedService;
                          Router.navigator.pushNamed(
                            Router.selectDateTimeScreen,
                            arguments: SelectDateTimeScreenArguments(
                                searchData: widget.searchData),
                          );
                        },
                        enabled: _selectedService != null,
                        text: 'Next',
                        disabledColor: Palette.iron,
                      ),
                    ),
                  ),
                  SizedBox(height: ScreenUtil().setHeight(23).toDouble()),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
