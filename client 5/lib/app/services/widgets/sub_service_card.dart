import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:security_company_app/app/common/consts/palette.dart';
import 'package:security_company_app/app/common/helpers/theme_helpers.dart';
import 'package:security_company_app/app/common/models/sec_service.dart';
import 'package:security_company_app/app/security_app/bloc/bloc.dart';

class SubServiceCard extends StatelessWidget {
  final int index;
  final SecService service;
  final bool selected;

  SubServiceCard(
      {@required this.index, @required this.service, this.selected = false});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AppBloc, AppState>(builder: (context, appState) {
      final theme = getThemeFromState(appState);

      return Container(
        constraints: BoxConstraints(
          maxWidth: double.infinity,
        ),
//        height: ScreenUtil().setHeight(93).toDouble(),
        margin: EdgeInsets.symmetric(
          horizontal: ScreenUtil().setWidth(7).toDouble(),
        ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12),
          color: selected ? Palette.sanMarino : Palette.alabaster,
          boxShadow: [
            BoxShadow(
              color: Palette.dullLavender.withOpacity(0.34),
              offset: Offset(1, 1),
              blurRadius: 3,
              spreadRadius: -1,
            ),
          ],
        ),
        child: Stack(
          children: <Widget>[
            Positioned(
              top: 0,
              right: 0,
              child: Transform.translate(
                offset: index % 2 == 0
                    ? Offset(ScreenUtil().setWidth(203.34 / 2).toDouble(), 0)
                    : Offset(ScreenUtil().setWidth(203.34 / 2).toDouble(),
                        -ScreenUtil().setWidth(196 / 2).toDouble()),
                child: Container(
                  width: ScreenUtil().setWidth(203.34).toDouble(),
                  height: ScreenUtil().setWidth(196).toDouble(),
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: selected
                        ? Colors.black.withOpacity(0.03)
                        : Palette.sanMarino.withOpacity(0.04),
                  ),
                ),
              ),
            ),
            Positioned(
              child: Container(
                margin: EdgeInsets.symmetric(
                  horizontal: ScreenUtil().setWidth(45).toDouble(),
                  vertical: ScreenUtil().setHeight(20).toDouble(),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      service.name,
                      style: selected
                          ? theme.selectedSubServiceNameStyle
                          : theme.subServiceNameStyle,
                    ),
                    SizedBox(height: ScreenUtil().setHeight(16).toDouble()),
                    Text(
                      service.description,
                      style: selected
                          ? theme.selectedSubServiceDescriptionStyle
                          : theme.subServiceDescriptionStyle,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      );
    });
  }
}
