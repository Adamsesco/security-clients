import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class SignUpEvent extends Equatable {
  const SignUpEvent();
}

class SignUpRequested extends SignUpEvent {
  final String email;
  final String password;
  final String firstName;
  final String lastName;

  SignUpRequested(
      {@required this.email,
      @required this.password,
      @required this.firstName,
      @required this.lastName});

  @override
  List<Object> get props => [email, password];
}
