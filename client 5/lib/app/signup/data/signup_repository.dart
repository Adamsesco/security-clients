import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:meta/meta.dart';
import 'package:security_company_app/app/common/models/client.dart';
import 'package:security_company_app/app/common/models/language.dart';
import 'package:security_company_app/app/common/models/user.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SignUpRepository {
  final String apiUrl;

  SignUpRepository({@required this.apiUrl});

  Future<User> signUp(
      String email, String password, String firstName, String lastName) async {
    final formData = FormData.fromMap({
      'email': email,
      'password': password,
      'firstname': firstName,
      'lastname': lastName,
    });
    final request = await Dio().post<String>(apiUrl, data: formData);

    if (request == null || request.statusCode != 200 || request.data == null) {
      print('Status code: ${request.statusCode}');
      print('Data: ${request.data}');
      return null;
    }

    final responseJson = jsonDecode(request.data);

    final code = responseJson['code'] as int;
    if (code == null || code != 1) {
      print('Code: $code');
      print('Data: ${request.data}');
      return null;
    }

    final userData = responseJson['data'];
    if (userData == null) {
      print('Data: ${request.data}');
      return null;
    }

    print('User data: $userData');

    final user = Client(
      id: userData['id'] as String,
      email: userData['email'] as String,
      token: userData['token'] as String,
      firstName: userData['firstname'] as String,
      lastName: userData['lastname'] as String,
      avatarUrl: userData['avatar'] as String,
      notificationsEnabled: true,
      language: Language.english,
    );
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("token", user.token);
    prefs.setString("id", user.id);
    prefs.setString("email", user.email);
    return user;
  }
}
