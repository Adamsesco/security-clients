import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';
import 'package:security_company_app/app/common/consts/palette.dart';
import 'package:security_company_app/app/common/models/bodyguard.dart';
import 'package:security_company_app/app/common/models/client.dart';
import 'package:security_company_app/app/common/models/company.dart';
import 'package:security_company_app/app/common/widgets/sec_back_button.dart';
import 'package:security_company_app/app/common/widgets/sec_primary_button.dart';
import 'package:security_company_app/app/routes/router.gr.dart';
import 'package:security_company_app/app/security_app/bloc/bloc.dart';
import 'package:security_company_app/app/signin/login_provider.dart';
import 'package:security_company_app/app/signin/share_preferences.dart';
import 'package:security_company_app/app/signup/bloc/bloc.dart';

class SignupScreen extends StatefulWidget {

  SignupScreen({Key key, @required this.user})
      : super(key: key);

  Client user;



  @override
  _SignupScreenState createState() => _SignupScreenState();
}

class _SignupScreenState extends State<SignupScreen> {
  SignUpBloc _signUpBloc;
  String _firstName;
  String _lastName;
  String _password;
  String _email;
  final TextEditingController _firstCtrl = TextEditingController();
  final TextEditingController _lastCtrl = TextEditingController();
  final TextEditingController _emailCtrl = TextEditingController();

  @override
  void initState() {
    super.initState();

    _signUpBloc = context.bloc<SignUpBloc>();

    if(widget.user != null){
      _firstName = widget.user.firstName;
      _lastName = widget.user.lastName;
      _email = widget.user.email;

      _firstCtrl.text = widget.user.firstName;
      _lastCtrl.text = widget.user.lastName;
      _emailCtrl.text = widget.user.email;
    }
  }

  void _signup() {
    if (_firstName == null ||
        _firstName.isEmpty ||
        _lastName == null ||
        _lastName.isEmpty ||
        _password == null ||
        _password.isEmpty ||
        _email == null ||
        _email.isEmpty) {
      showCupertinoDialog(
        context: context,
        builder: (context) => CupertinoAlertDialog(
          content: Text('Please fill in all the fields.'),
          actions: <Widget>[
            CupertinoDialogAction(
              onPressed: () => Navigator.of(context).pop(),
              child: Text('Ok'),
            ),
          ],
        ),
      );
      return;
    }

    _signUpBloc.add(SignUpRequested(
        email: _email,
        password: _password,
        firstName: _firstName,
        lastName: _lastName));
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AppBloc, AppState>(
        builder: (BuildContext context, AppState state) {
      final appState = state as AppLoadSuccess;

      return BlocConsumer<SignUpBloc, SignUpState>(
          listener: (BuildContext context, SignUpState signUpState) {
        if (signUpState is SignUpFailure) {
          final message = signUpState.errorMessage;

          showCupertinoDialog(
            context: context,
            builder: (context) => CupertinoAlertDialog(
              title: Text('Error'),
              content: Text(message),
              actions: <Widget>[
                CupertinoDialogAction(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text('Ok'),
                ),
              ],
            ),
          );
        } else if (signUpState is SignUpSuccess) {
          final user = signUpState.user;

          StorageUtil.inicializar().then((_) {
            var myProvider = Provider.of<LoginProvider>(context, listen: false);

            myProvider.mitexto = 'Provider';
            myProvider.isAuthenticate = true;
            StorageUtil.putBool('isAutenticado', true);

            Navigator.pop(context);
            if(widget.user == null) {
              Navigator.pop(context);
            }
          });
        }
      }, builder: (context, snapshot) {
        return Scaffold(
          body: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [Palette.sanMarino, Palette.cello],
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
              ),
            ),
            child: SafeArea(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(height: ScreenUtil().setHeight(10).toDouble()),
                  Row(
                    children: <Widget>[
                      SizedBox(width: ScreenUtil().setWidth(29).toDouble()),
                      SecBackButton(),
                    ],
                  ),
                  SizedBox(height: ScreenUtil().setHeight(19).toDouble()),
                  Padding(
                    padding: EdgeInsetsDirectional.only(
                      start: ScreenUtil().setWidth(39).toDouble(),
                    ),
                    child: Text(
                      'Create new account',
                      style: appState.theme.titleStyle,
                    ),
                  ),
                  SizedBox(height: ScreenUtil().setHeight(28).toDouble()),
                  Expanded(
                    child: Container(
                      constraints: BoxConstraints(
                        minWidth: double.infinity,
                      ),
                      padding: EdgeInsets.symmetric(
                        horizontal: ScreenUtil().setWidth(31).toDouble(),
                        vertical: ScreenUtil().setHeight(34).toDouble(),
                      ),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(20),
                          topRight: Radius.circular(20),
                        ),
                      ),
                      child: Column(
                        children: <Widget>[
                          Align(
                            alignment: AlignmentDirectional.topStart,
                            child: Text(
                              'Welcome',
                              style: appState.theme.headStyle,
                            ),
                          ),
                          SizedBox(
                              height: ScreenUtil().setHeight(5).toDouble()),
                          Align(
                            alignment: AlignmentDirectional.topStart,
                            child: Text(
                              'Sign up to continue',
                              style: appState.theme.subheadStyle,
                            ),
                          ),
                          Spacer(),
                          TextField(
                            controller: _firstCtrl,
                            onChanged: (String value) {
                              _firstName = value;
                            },
                            decoration: InputDecoration(
                              contentPadding: EdgeInsets.only(
                                bottom: ScreenUtil().setHeight(30.5).toDouble(),
                              ),
                              hintText: 'First name',
                              hintStyle: TextStyle(
                                fontFamily: 'Rubik',
                                fontSize: ScreenUtil().setSp(14).toDouble(),
                                color: Color(0xFFB8BCC7),
                              ),
                              border: UnderlineInputBorder(
                                borderSide: BorderSide(
                                  color: Color(0xFFD8D8D8),
                                ),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(
                                  color: Color(0xFFD8D8D8),
                                ),
                              ),
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(
                                  color: Color(0xFFD8D8D8),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                              height: ScreenUtil().setHeight(30.5).toDouble()),
                          TextField(
                            controller: _lastCtrl,

                            onChanged: (String value) {
                              _lastName = value;
                            },
                            decoration: InputDecoration(
                              contentPadding: EdgeInsets.only(
                                bottom: ScreenUtil().setHeight(30.5).toDouble(),
                              ),
                              hintText: 'Last name',
                              hintStyle: TextStyle(
                                fontFamily: 'Rubik',
                                fontSize: ScreenUtil().setSp(14).toDouble(),
                                color: Color(0xFFB8BCC7),
                              ),
                              border: UnderlineInputBorder(
                                borderSide: BorderSide(
                                  color: Color(0xFFD8D8D8),
                                ),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(
                                  color: Color(0xFFD8D8D8),
                                ),
                              ),
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(
                                  color: Color(0xFFD8D8D8),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                              height: ScreenUtil().setHeight(30.5).toDouble()),
                          TextField(
                            onChanged: (String value) {
                              _password = value;
                            },
                            obscureText: true,
                            decoration: InputDecoration(
                              contentPadding: EdgeInsets.only(
                                bottom: ScreenUtil().setHeight(30.5).toDouble(),
                              ),
                              hintText: 'Password',
                              hintStyle: TextStyle(
                                fontFamily: 'Rubik',
                                fontSize: ScreenUtil().setSp(14).toDouble(),
                                color: Color(0xFFB8BCC7),
                              ),
                              border: UnderlineInputBorder(
                                borderSide: BorderSide(
                                  color: Color(0xFFD8D8D8),
                                ),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(
                                  color: Color(0xFFD8D8D8),
                                ),
                              ),
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(
                                  color: Color(0xFFD8D8D8),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                              height: ScreenUtil().setHeight(30.5).toDouble()),
                          TextField(
                            controller: _emailCtrl,

                            onChanged: (String value) {
                              _email = value;
                            },
                            decoration: InputDecoration(
                              contentPadding: EdgeInsets.only(
                                bottom: ScreenUtil().setHeight(30.5).toDouble(),
                              ),
                              hintText: 'Email address',
                              hintStyle: TextStyle(
                                fontFamily: 'Rubik',
                                fontSize: ScreenUtil().setSp(14).toDouble(),
                                color: Color(0xFFB8BCC7),
                              ),
                              border: UnderlineInputBorder(
                                borderSide: BorderSide(
                                  color: Color(0xFFD8D8D8),
                                ),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(
                                  color: Color(0xFFD8D8D8),
                                ),
                              ),
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(
                                  color: Color(0xFFD8D8D8),
                                ),
                              ),
                            ),
                          ),
                          Spacer(),
                          SizedBox(
                            width: ScreenUtil().setWidth(248).toDouble(),
                            child: Text(
                              'By creating an account, you agree to our terms & conditions',
                              style: TextStyle(
                                fontFamily: 'Rubik',
                                fontSize: ScreenUtil().setSp(13).toDouble(),
                                color: Color(0xFFB8BCC7),
                              ),
                            ),
                          ),
                          Spacer(),
                          SecPrimaryButton(
                            color: Color(0xFF4361BA),
                            text: 'Sign up',
                            onTap: _signup,
                          ),
                          Spacer(),
                          InkWell(
                            onTap: () {
                              Router.navigator.pushNamed(Router.signinScreen);
                            },
                            child: RichText(
                              text: TextSpan(
                                style: TextStyle(
                                  fontFamily: 'Rubik',
                                  fontSize: ScreenUtil().setSp(13).toDouble(),
                                  color: Color(0xFFB8BCC7),
                                ),
                                text: 'Already have an account ? ',
                                children: [
                                  TextSpan(
                                    text: 'Sign in',
                                    style: TextStyle(
                                      fontWeight: FontWeight.w700,
                                      color: Color(0xFF4361BA),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      });
    });
  }
}
