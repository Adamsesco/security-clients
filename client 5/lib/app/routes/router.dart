import 'package:auto_route/auto_route_annotations.dart';
import 'package:security_company_app/app/calendar/screens/select_date_time_screen.dart';
import 'package:security_company_app/app/dashboard/screens/client_dashboard_screen.dart';
import 'package:security_company_app/app/guards_number/screens/guards_number_screen.dart';
import 'package:security_company_app/app/home/screens/home_screen.dart';
import 'package:security_company_app/app/home/screens/main_screen.dart';
import 'package:security_company_app/app/intro/screens/intro_screen.dart';
import 'package:security_company_app/app/location/screens/location_screen.dart';
import 'package:security_company_app/app/notifications/screens/notifications_screen.dart';
import 'package:security_company_app/app/search/screens/search_results_screen.dart';
import 'package:security_company_app/app/services/screens/select_service_screen.dart';
import 'package:security_company_app/app/settings/screens/settings_screen.dart';
import 'package:security_company_app/app/signin/screens/signin_screen.dart';
import 'package:security_company_app/app/signup/screens/signup_screen.dart';

@autoRouter
class $Router {
  @initial
  HomeScreen homeScreen;

  SigninScreen signinScreen;

  SignupScreen signupScreen;

  IntroScreen introScreen;

  MainScreen mainScreen;

  LocationScreen locationScreen;

  SelectServiceScreen selectServiceScreen;

  SelectDateTimeScreen selectDateTimeScreen;

  GuardsNumberScreen guardsNumberScreen;

  SearchResultsScreen searchResultsScreen;

  ClientDashboardScreen clientDashboardScreen;


  SettingsScreen settingsScreen;

  NotificationsScreen notificationsScreen;
}
