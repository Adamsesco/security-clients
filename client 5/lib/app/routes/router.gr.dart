// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:auto_route/router_utils.dart';
import 'package:security_company_app/app/common/models/client.dart';
import 'package:security_company_app/app/home/screens/home_screen.dart';
import 'package:security_company_app/app/signin/screens/signin_screen.dart';
import 'package:security_company_app/app/signup/screens/signup_screen.dart';
import 'package:security_company_app/app/intro/screens/intro_screen.dart';
import 'package:security_company_app/app/home/screens/main_screen.dart';
import 'package:security_company_app/app/location/screens/location_screen.dart';
import 'package:security_company_app/app/common/models/search_data.dart';
import 'package:security_company_app/app/services/screens/select_service_screen.dart';
import 'package:security_company_app/app/calendar/screens/select_date_time_screen.dart';
import 'package:security_company_app/app/guards_number/screens/guards_number_screen.dart';
import 'package:security_company_app/app/search/screens/search_results_screen.dart';
import 'package:security_company_app/app/dashboard/screens/client_dashboard_screen.dart';
import 'package:security_company_app/app/settings/screens/settings_screen.dart';
import 'package:security_company_app/app/notifications/screens/notifications_screen.dart';

class Router {
  static const homeScreen = '/';
  static const signinScreen = '/signin-screen';
  static const signupScreen = '/signup-screen';
  static const introScreen = '/intro-screen';
  static const mainScreen = '/main-screen';
  static const locationScreen = '/location-screen';
  static const selectServiceScreen = '/select-service-screen';
  static const selectDateTimeScreen = '/select-date-time-screen';
  static const guardsNumberScreen = '/guards-number-screen';
  static const searchResultsScreen = '/search-results-screen';
  static const clientDashboardScreen = '/client-dashboard-screen';
  static const companyDashboardScreen = '/company-dashboard-screen';
  static const settingsScreen = '/settings-screen';
  static const notificationsScreen = '/notifications-screen';

  static GlobalKey<NavigatorState> get navigatorKey =>
      getNavigatorKey<Router>();

  static NavigatorState get navigator => navigatorKey.currentState;

  static Route<dynamic> onGenerateRoute(RouteSettings settings) {
    final args = settings.arguments;
    switch (settings.name) {
      case Router.homeScreen:
        return MaterialPageRoute(
          builder: (_) => HomeScreen(),
          settings: settings,
        );


      case Router.signinScreen:
        return MaterialPageRoute(
          builder: (_) => SigninScreen(/*go_to: typedArgs.go_to,*/),
          settings: settings,
        );



      case Router.signupScreen:
        if (hasInvalidArgs<SignupScreenArguments>(args, isRequired: true)) {
          return misTypedArgsRoute<SignupScreenArguments>(args);
        }
        final typedArgs = args as SignupScreenArguments;
        return MaterialPageRoute(
          builder: (_) =>
              SignupScreen(key: typedArgs.key, user: typedArgs.user),
          settings: settings,
        );


      case Router.introScreen:
        return MaterialPageRoute(
          builder: (_) => IntroScreen(),
          settings: settings,
        );
      case Router.mainScreen:
        if (hasInvalidArgs<MainScreenArguments>(args, isRequired: true)) {
          return misTypedArgsRoute<MainScreenArguments>(args);
        }
        final typedArgs = args as MainScreenArguments;
        return MaterialPageRoute(
          builder: (_) => MainScreen(returnn: typedArgs.returnn,),
          settings: settings,
        );
      case Router.locationScreen:
        if (hasInvalidArgs<LocationScreenArguments>(args, isRequired: true)) {
          return misTypedArgsRoute<LocationScreenArguments>(args);
        }
        final typedArgs = args as LocationScreenArguments;
        return MaterialPageRoute(
          builder: (_) => LocationScreen(
                  key: typedArgs.key, searchData: typedArgs.searchData)
              .wrappedRoute,
          settings: settings,
        );
      case Router.selectServiceScreen:
        if (hasInvalidArgs<SelectServiceScreenArguments>(args,
            isRequired: true)) {
          return misTypedArgsRoute<SelectServiceScreenArguments>(args);
        }
        final typedArgs = args as SelectServiceScreenArguments;
        return MaterialPageRoute(
          builder: (_) => SelectServiceScreen(
              key: typedArgs.key, searchData: typedArgs.searchData),
          settings: settings,
        );
      case Router.selectDateTimeScreen:
        if (hasInvalidArgs<SelectDateTimeScreenArguments>(args,
            isRequired: true)) {
          return misTypedArgsRoute<SelectDateTimeScreenArguments>(args);
        }
        final typedArgs = args as SelectDateTimeScreenArguments;
        return MaterialPageRoute(
          builder: (_) => SelectDateTimeScreen(
              key: typedArgs.key, searchData: typedArgs.searchData),
          settings: settings,
        );
      case Router.guardsNumberScreen:
        if (hasInvalidArgs<GuardsNumberScreenArguments>(args,
            isRequired: true)) {
          return misTypedArgsRoute<GuardsNumberScreenArguments>(args);
        }
        final typedArgs = args as GuardsNumberScreenArguments;
        return MaterialPageRoute(
          builder: (_) => GuardsNumberScreen(
              key: typedArgs.key, searchData: typedArgs.searchData),
          settings: settings,
        );
      case Router.searchResultsScreen:
        if (hasInvalidArgs<SearchResultsScreenArguments>(args,
            isRequired: true)) {
          return misTypedArgsRoute<SearchResultsScreenArguments>(args);
        }
        final typedArgs = args as SearchResultsScreenArguments;
        return MaterialPageRoute(
          builder: (_) => SearchResultsScreen(
              key: typedArgs.key, searchData: typedArgs.searchData),
          settings: settings,
        );
      case Router.clientDashboardScreen:
        if (hasInvalidArgs<Key>(args)) {
          return misTypedArgsRoute<Key>(args);
        }
        final typedArgs = args as Key;
        return MaterialPageRoute(
          builder: (_) => ClientDashboardScreen(key: typedArgs),
          settings: settings,
        );
      case Router.settingsScreen:
        if (hasInvalidArgs<Key>(args)) {
          return misTypedArgsRoute<Key>(args);
        }
        final typedArgs = args as Key;
        return MaterialPageRoute(
          builder: (_) => SettingsScreen(key: typedArgs),
          settings: settings,
        );
      case Router.notificationsScreen:
        if (hasInvalidArgs<Key>(args)) {
          return misTypedArgsRoute<Key>(args);
        }
        final typedArgs = args as Key;
        return MaterialPageRoute(
          builder: (_) => NotificationsScreen(key: typedArgs),
          settings: settings,
        );
      default:
        return unknownRoutePage(settings.name);
    }
  }
}

//**************************************************************************
// Arguments holder classes
//***************************************************************************

//LocationScreen arguments holder class
class LocationScreenArguments {
  final Key key;
  final SearchData searchData;

  LocationScreenArguments({this.key, @required this.searchData});
}


class MainScreenArguments {
  final Key key;
  final bool returnn;

  MainScreenArguments({this.key, @required this.returnn});
}
class SignupScreenArguments {
  final Key key;
  final Client user;

  SignupScreenArguments({this.key, @required this.user});
}

class SigninScreenArguments {
  final Key key;
  final String go_to;

  SigninScreenArguments({this.key, @required this.go_to});
}

//SelectServiceScreen arguments holder class
class SelectServiceScreenArguments {
  final Key key;
  final SearchData searchData;

  SelectServiceScreenArguments({this.key, @required this.searchData});
}

//SelectDateTimeScreen arguments holder class
class SelectDateTimeScreenArguments {
  final Key key;
  final SearchData searchData;

  SelectDateTimeScreenArguments({this.key, @required this.searchData});
}

//GuardsNumberScreen arguments holder class
class GuardsNumberScreenArguments {
  final Key key;
  final SearchData searchData;

  GuardsNumberScreenArguments({this.key, @required this.searchData});
}

//SearchResultsScreen arguments holder class
class SearchResultsScreenArguments {
  final Key key;
  final SearchData searchData;

  SearchResultsScreenArguments({this.key, @required this.searchData});
}
