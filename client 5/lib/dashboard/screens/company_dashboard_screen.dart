import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:security_company_app/app/common/consts/palette.dart';
import 'package:security_company_app/app/common/helpers/theme_helpers.dart';
import 'package:security_company_app/app/common/models/bodyguard.dart';
import 'package:security_company_app/app/common/models/company.dart';
import 'package:security_company_app/app/common/models/door_supervision_sub_service.dart';
import 'package:security_company_app/app/common/models/guard_type.dart';
import 'package:security_company_app/app/common/models/manned_security_sub_service.dart';
import 'package:security_company_app/app/common/models/order_status.dart';
import 'package:security_company_app/app/common/models/static_security_sub_service.dart';
import 'package:security_company_app/app/common/widgets/sec_screen_subtitle.dart';
import 'package:security_company_app/app/dashboard/widgets/order_card.dart';
import 'package:security_company_app/app/models/dahsboard.dart';
import 'package:security_company_app/app/security_app/bloc/bloc.dart';

class CompanyDashboardScreen extends StatefulWidget {
  @override
  _CompanyDashboardScreenState createState() => _CompanyDashboardScreenState();
}

class _CompanyDashboardScreenState extends State<CompanyDashboardScreen> {
  DateTime _selectedDate;
  List<Order> _orders;
  int _expandedIndex;

  @override
  void initState() {
    super.initState();

    _selectedDate = DateTime.now();

   /* _orders = [
      Order(
        id: 'SG0001',
        status: OrderStatus.newOrder,
        company: Company(
          id: '1',
          name: 'ThunderSec',
          available: true,
          phoneNumber: '+4456543256',
        ),
        address: 'London, UK',
        dateCreated: DateTime(2020, 2, 27, 12, 30, 43),
        dateFrom: DateTime(2020, 2, 28, 8),
        dateTo: DateTime(2020, 3, 5),
        guardType: GuardType.securityGuard,
        guardsCount: 2,
        service: MannedSecuritySubService(),
        price: 120,
      ),
      Order(
        id: 'BG0003',
        status: OrderStatus.pending,
        bodyguard: Bodyguard(
          id: '5',
          name: 'Brian Norton',
          available: true,
          phoneNumber: '+4456543256',
        ),
        address: 'London, UK',
        dateCreated: DateTime(2020, 2, 28, 12, 30, 43),
        dateFrom: DateTime(2020, 2, 28, 8),
        dateTo: DateTime(2020, 3, 5),
        guardType: GuardType.bodyGuard,
        guardsCount: 1,
        service: StaticSecuritySubService(),
        price: 80,
      ),
      Order(
        id: 'SG0005',
        status: OrderStatus.inProgress,
        company: Company(
          id: '1',
          name: 'ThunderSec',
          available: true,
          phoneNumber: '+4456543256',
        ),
        address: 'London, UK',
        dateCreated: DateTime(2020, 2, 27, 12, 30, 43),
        dateFrom: DateTime(2020, 3, 2, 8),
        dateTo: DateTime(2020, 3, 5),
        guardType: GuardType.securityGuard,
        guardsCount: 2,
        service: DoorSupervisionSubService(),
        price: 120,
      ),
      Order(
        id: 'SG0005',
        status: OrderStatus.done,
        company: Company(
          id: '1',
          name: 'ThunderSec',
          available: true,
          phoneNumber: '+4456543256',
        ),
        address: 'London, UK',
        dateCreated: DateTime(2020, 2, 27, 12, 30, 43),
        dateFrom: DateTime(2020, 3, 2, 8),
        dateTo: DateTime(2020, 3, 5),
        guardType: GuardType.securityGuard,
        guardsCount: 2,
        service: DoorSupervisionSubService(),
        price: 120,
      ),
    ];*/
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AppBloc, AppState>(
      builder: (context, appState) {
        final theme = getThemeFromState(appState);

        return Scaffold(
          body: Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/images/background.png'),
                fit: BoxFit.cover,
                alignment: Alignment.topRight,
              ),
              color: Palette.mystic,
            ),
            child: SafeArea(
              child: Column(
                children: <Widget>[
                  SizedBox(height: ScreenUtil().setHeight(26).toDouble()),
                  Container(
                    alignment: AlignmentDirectional.topEnd,
                    margin: EdgeInsetsDirectional.only(
                      end: ScreenUtil().setWidth(21).toDouble(),
                    ),
                    child: Text(
                      'DASHBOARD',
                      style: theme.screenTitleStyle,
                    ),
                  ),
                  SizedBox(height: ScreenUtil().setHeight(18).toDouble()),
                  Align(
                    alignment: AlignmentDirectional.topStart,
                    child: SecScreenSubtitle(
                      subtitle:
                          'The data displayed on your dashboard is monthly',
                      small: true,
                    ),
                  ),
                  SizedBox(height: ScreenUtil().setHeight(20.32).toDouble()),
                  Container(
                    margin: EdgeInsets.symmetric(
                        horizontal: ScreenUtil().setWidth(15).toDouble()),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        InkWell(
                          child: Padding(
                            padding: EdgeInsets.symmetric(
                              horizontal: ScreenUtil().setWidth(10).toDouble(),
                            ),
                            child: Row(
                              children: <Widget>[
                                Text(
                                  DateFormat('MMMM').format(_selectedDate),
                                  style: theme.dateScreenDateStyle.copyWith(
                                    fontSize: ScreenUtil().setSp(19).toDouble(),
                                  ),
                                ),
                                SizedBox(
                                    width: ScreenUtil().setWidth(8).toDouble()),
                                Image.asset(
                                    'assets/images/dropdown-button-icon.png'),
                              ],
                            ),
                          ),
                        ),
                        SizedBox(height: ScreenUtil().setHeight(10).toDouble()),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Container(
                              width: ScreenUtil().setWidth(107).toDouble(),
                              height: ScreenUtil().setWidth(107).toDouble(),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(12),
                                color: Colors.white,
                              ),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    '54',
                                    style: TextStyle(
                                      fontFamily: 'Rubik',
                                      fontSize:
                                          ScreenUtil().setSp(36).toDouble(),
                                      fontWeight: FontWeight.w700,
                                      color: Palette.sanMarino,
                                    ),
                                  ),
                                  Text(
                                    'COMPLETED',
                                    style: TextStyle(
                                      fontFamily: 'Rubik',
                                      fontSize:
                                          ScreenUtil().setSp(13).toDouble(),
                                      fontWeight: FontWeight.w700,
                                      color: Palette.sanMarino,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              width: ScreenUtil().setWidth(107).toDouble(),
                              height: ScreenUtil().setWidth(107).toDouble(),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(12),
                                gradient: LinearGradient(
                                  colors: [Palette.crusta, Palette.supernova],
                                  begin: Alignment.topCenter,
                                  end: Alignment.bottomCenter,
                                  stops: [0.0, 2.0],
                                ),
                              ),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    '03',
                                    style: TextStyle(
                                      fontFamily: 'Rubik',
                                      fontSize:
                                          ScreenUtil().setSp(36).toDouble(),
                                      fontWeight: FontWeight.w700,
                                      color: Colors.white,
                                    ),
                                  ),
                                  Text(
                                    'PENDING',
                                    style: TextStyle(
                                      fontFamily: 'Rubik',
                                      fontSize:
                                          ScreenUtil().setSp(13).toDouble(),
                                      fontWeight: FontWeight.w700,
                                      color: Colors.white,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              width: ScreenUtil().setWidth(107).toDouble(),
                              height: ScreenUtil().setWidth(107).toDouble(),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(12),
                                gradient: LinearGradient(
                                  colors: [Palette.milanoRed, Colors.red],
                                  begin: Alignment.topCenter,
                                  end: Alignment.bottomCenter,
                                ),
                              ),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    '12',
                                    style: TextStyle(
                                      fontFamily: 'Rubik',
                                      fontSize:
                                          ScreenUtil().setSp(36).toDouble(),
                                      fontWeight: FontWeight.w700,
                                      color: Colors.white,
                                    ),
                                  ),
                                  Text(
                                    'CANCELED',
                                    style: TextStyle(
                                      fontFamily: 'Rubik',
                                      fontSize:
                                          ScreenUtil().setSp(13).toDouble(),
                                      fontWeight: FontWeight.w700,
                                      color: Colors.white,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: ScreenUtil().setHeight(22).toDouble()),
                  Container(
                    margin: EdgeInsetsDirectional.only(
                      start: ScreenUtil().setWidth(15).toDouble(),
                      end: ScreenUtil().setWidth(17).toDouble(),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          'Orders',
                          style: TextStyle(
                            fontFamily: 'Rubik',
                            fontSize: ScreenUtil().setSp(20).toDouble(),
                            fontWeight: FontWeight.w700,
                            color: Color(0xFFA8B7E3),
                          ),
                        ),
                        InkWell(
                          child: Padding(
                            padding: EdgeInsets.symmetric(
                              horizontal: ScreenUtil().setWidth(10).toDouble(),
                            ),
                            child: Row(
                              children: <Widget>[
                                Text(
                                  'Sort by',
                                  style: theme.dateScreenDateStyle,
                                ),
                                SizedBox(
                                    width: ScreenUtil().setWidth(7).toDouble()),
                                Image.asset(
                                    'assets/images/dropdown-button-icon.png'),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: ScreenUtil().setHeight(13).toDouble()),
                  Expanded(
                    child: ListView.separated(
                      padding: EdgeInsets.symmetric(
                        horizontal: ScreenUtil().setWidth(7).toDouble(),
                      ),
                      itemCount: _orders?.length ?? 0,
                      separatorBuilder: (_, __) => SizedBox(
                          height: ScreenUtil().setHeight(5).toDouble()),
                      itemBuilder: (BuildContext context, int index) {
                        final order = _orders[index];

                        return OrderCard(
                          order: order,
                          expanded: _expandedIndex == index,
                          onTap: () {
                            setState(() {
                              if (_expandedIndex == index) {
                                _expandedIndex = null;
                              } else {
                                _expandedIndex = index;
                              }
                            });
                          },
                        );
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
